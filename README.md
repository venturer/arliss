# README #

Project for ARLISS (A Rocket Launch for International Students Satellites)

The ARLISS Project is a collaborative effort between students and faculty at 
Stanford University Space Systems Development Program and other educational institutions, 
and high power rocketry enthusiasts in Northern California, to build, launch, 
test and recover prototype autonomous rovers that can drive to an specified GPS coordinate.

The project was developed by Roy Araya (Leader), Daniel Garbanzo (Electronics Specialist), Jorge Carvajal (Software).

In this repository you can find the source code used on the rover, as well as the mechanical and electrical design.

![IMG_20150529_103253-min.jpg](https://bitbucket.org/repo/bKb9jk/images/2988940543-IMG_20150529_103253-min.jpg)

