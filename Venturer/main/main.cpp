
 /* main.cpp* 
 * 
 * Modifications made by:
 * Roy Araya, Mechatronics Engineering Student
 * Daniel Garbanzo, Electronics Engineering Student
 * Jorge Carvajal, Computers Engineering Student
 * 
 * royaraya16@gmail.com
 * 
 * Based on James Strawson's Robotics Cape code.
 * 
 	
Copyright (c) 2015, Roy Araya
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

#include "main.h"

int main(int argc, char *argv[]){
	parse_args(argc, argv);
	ROVER_ON();
	//get_Distance();
	
	if(ROVER_flags[AYUDA]){
		print_usage();
		return 0;
	}
	while(get_state() != EXITING){
		update_Rover();
		usleep(wait_us);
	}

	ROVER_OFF();

	return 0;
}

int ROVER_ON(){
	
	//exportar e inicializar los GPIO
	init_GPIO();
	
	//inicializar los PWM y crear punteros para el ciclo de trabajo
	init_PWM();
	
	//inicializar eQEP
	//init_eQEP();
	
	//Inicializar comunicacion serial
	initGPSSerial();
	initBLUETOOTH_Serial();
	
	//inicializar GPS
	init_GPS();
	
	//Iniciando PID
	reset_PID();
	
	//setPID(0.02,0.00004485,0.0);
	setPID(0.002,0.00004485,0.0);
	rover.tope_PWM = 1.0;
	
	//Poniendo contadores en cero
	rover.stuckCount = 0;
	rover.arriveCount = 0;
	rover.deployCount = 0;
	
	//Coordenadas de la META	
	rover.latGoal = LAB_LATITUDE;
	rover.lonGoal = LAB_LONGITUDE;
	
	//Hilo de sintonia fina
	if(ROVER_flags[CALIBRACION]){
		pthread_t CALIBRACION_thread;
		pthread_create(&CALIBRACION_thread, NULL, bluetooth_PID_tunning, (void*) NULL);
	}
	
	//Preguntar si se inicia en modo control remoto	
	else if(ROVER_flags[CONTROL_REMOTO]){
		pthread_t BLUETOOTH_thread;
		pthread_create(&BLUETOOTH_thread, NULL, bluetooth_remote_control, (void*) NULL);
	}
	
	//Hilo de graficacion de variables por bluetooth
	else if(ROVER_flags[DEBUG_BLUETOOTH]){
		pthread_t serial_thread;
		pthread_create(&serial_thread, NULL, send_Serial, (void*) NULL);
	}

	//Iniciar hilo GPS
	pthread_t GPS_thread;
	pthread_create(&GPS_thread, NULL, GPS_monitor, (void*) NULL);

	//Iniciar hilo IMU
	pthread_t IMU_thread;
	pthread_create(&IMU_thread, NULL, IMU_monitor, (void*) NULL);
	
	//Desactivar motores por si las moscas
	disable_motors();
	rover.PWMLeft = 0;
	rover.PWMRight = 0;
	
	// Start Signal Handler
	signal(SIGINT, ctrl_c);
	
	//Filtro FIR
	median = MediatorNew(30);

	//Inicializando variable de aceleracion
	rover.last_acelmag = 0;
	
	set_state(LAUNCH);

	printf("---------\nETAPA DE INICIALIZACION\n---------\n");
	sprintf(STATE_str, "---------\nETAPA DE INICIALIZACION\n---------\n");
    	XBEE_SerialWrite(STATE_str);
    
	printf("---------\nROVER ON\n---------\n");
	
	return 0;
}

void update_Rover(){
	
	//Actualizando datos de GPS
	update_GPS();
	
	//Actualizando datos del IMU
	update_IMU();	
    
    //Descomentar para el debug
	
	//actualizacion del error 
	/*rover.error =  rover.azimuth1_2Goal - rover.Compass_Heading;
	if(rover.error > 180.0)
		rover.error = -(360.0 - rover.error);
	else if(rover.error < -180.0)
		rover.error = rover.error  + 360.0;
	rover.error = -rover.error;*/
	
	if(!ROVER_flags[CONTROL_REMOTO]) //Pasar al manejo de estados si no se corrio en modo control remoto
		
		//Dependiendo del estado asi se van a manejar los motores
		
		switch(get_state()){
		
		case LAUNCH:
		     //Se verifica si se encuentra en la etapa de lanzamiento, al detectar el ascenso y/o descenso con un delta de ACEL y AlT
		     //Si se detecta el lanzamiento, se pasa a la etapa LANDING.
		     
		     //if ((rover.ac_delta > maxdelta_normacel) && (rover.height > lim_topalt)){
		     if (rover.height > lim_topalt){ //si se queda quedito por 30 min pasar a LANDING
			 
			 set_state(LANDING);
			 
			 //printf("---------\nETAPA DE LANZAMIENTO\n---------\n");
			 sprintf(STATE_str, "---------\nETAPA DE LANZAMIENTO\n---------\n");
			 XBEE_SerialWrite(STATE_str);			 
		     }
		     
		     break;	
		
		case LANDING:
			//Espera y verifica si el ROVER ya aterrizó. Para ello verifica que la aceleración y altura vuelvan a sus valores de referencia.
			//Una vez que ha aterrizado, ejecuta la rutina de separacion de paracaidas y pasa al estado DEPLOYMENT.

			//Si se mueve resetar el contador
			if((-maxaceldelta_rep > rover.ac_delta) || (rover.ac_delta > maxaceldelta_rep)){
				rover.arriveCount = 0;
			}

			//if (((ground_level_ref - 5.0) < rover.height) && (rover.height < (ground_level_ref + 5.0))){
			if ((rover.height < (ground_level_ref + 2.0)) || rover.arriveCount > 4000){			
				
				//Si duro 10s sin salirse del rango minimo de aceleracion pasar a la siguiente etapa
				if(rover.arriveCount > 200){
					
					set_state(DEPLOYMENT);
									
					//RUTINA DE SEPARACIÓN DEL PARACAIDAS
					printf("---------\nETAPA DE ATERRIZAJE CONCLUIDA\n---------\n");
					sprintf(STATE_str, "---------\nETAPA DE ATERRIZAJE CONCLUIDA\n---------\n");
					XBEE_SerialWrite(STATE_str);
					printf("---------\nINICIO DE NAVEGACIÓN\n---------\n");
					sprintf(STATE_str, "---------\nINICIO DE NAVEGACION\n---------\n");
					XBEE_SerialWrite(STATE_str);					 
				}			
			}

			rover.arriveCount++;
			break;
			
		case DEPLOYMENT:
			
			rover.PWMRight = 1.0;
			rover.PWMLeft = 1.0;
			
			if(rover.deployCount > 50){
				rover.PWMRight = 0;
				rover.PWMLeft = 0;
				rover.deployCount = 0;	
				set_state(NAVIGATION);
			}
			
			rover.deployCount++;
		
			break;
		
		case NAVIGATION: 
			//El Rover comienza a dirigirse hacia la meta.
			//Se debe verificar si hay atascamiento del ROVER en todo momento, de presentarse se brinca al estado STUCK.
			
			if(!PID.resetFLAG){
				reset_PID();
				PID.resetFLAG = 1;
			}
			
			//Se navega solo si hay FIX por parte del GPS
			
			if(gps.Fix != 0){
				
				//Calculo del error para la entrada del control
				rover.error =  rover.azimuth1_2Goal - rover.Compass_Heading;
				
				//Correcciones cuando el azimuth se encuentra en los cuadrantes 3 y 4
				if(rover.error > 180.0)
					rover.error = -(360.0 - rover.error);
					
				else if(rover.error < -180.0)
					rover.error = rover.error  + 360.0;
				
				//Cambio de signo al error, todavia no se porque
				rover.error = -rover.error;

				//Para que se vaya siempre al norte
				//rover.error = rover.Compass_Heading;

				//Algoritmo de control	
				roverNavigation();
				
				//Si esta a una distancia menor a 2m a la redonda de la meta apaguese
				//Se ponen 2m debido a la incertidumbre del GPS
				 
				if(rover.distance2Goal <= 2.0)
					set_state(EXITING);
			}
			
			break;

		case STUCK:
		
			//Rutina de desatascamiento
			//El rover ira en reversa al maximo de PWM durante 1s
			
			if(rover.stuckCount < 20){
				rover.PWMLeft = -1;
				rover.PWMRight = -1;
				rover.stuckCount++;
			}

			else{
				rover.stuckCount = 0;
				set_state(NAVIGATION);
				PID.resetFLAG = 0;
			}
			
			break;
			
		case EXITING:
			printf("Ya llegue!\n");
			return;
			break;

		case STANDBY:
			rover.PWMLeft = 0.0;
			rover.PWMRight = 0.0;
			break;
		
		default:
			break;
		}
		
		//Actualizando datos PWM y enviandolos al puente H
		
		set_motor(1, rover.PWMLeft);
		set_motor(2, rover.PWMRight);

		//printf("Left: %f \t Right: %f \n", rover.PWMLeft, rover.PWMRight);
}

int update_GPS(){
	
	if(gps.Fix != 0){
		geod_inverse(&g, gps.latitudeDD, gps.longitudeDD, rover.latGoal, rover.lonGoal, &rover.distance2Goal, &rover.azimuth1_2Goal, &rover.azimuth2_2Goal);

		if(rover.azimuth1_2Goal > 180)
			rover.azimuth1_2Goal = rover.azimuth1_2Goal - 360;
	}
	
	else
		return 1;
	
	return 0;
}

int update_IMU(){
	
	//Datos de aceleracion
	rover.accx = g_imuData.accel.x();
	rover.accy = g_imuData.accel.y();
	rover.accz = g_imuData.accel.z();
	
	rover.last_acelmag = rover.acelmag;	

	rover.acelmag = sqrt(pow(rover.accx,2) + pow(rover.accy,2) + pow(rover.accz,2));
	rover.ac_delta = rover.acelmag - rover.last_acelmag;

	//Datos de Attitude
	rover.fusx = g_imuData.fusionPose.x() * RTMATH_RAD_TO_DEGREE;
	rover.fusy = g_imuData.fusionPose.y() * RTMATH_RAD_TO_DEGREE;
	rover.fusz = g_imuData.fusionPose.z() * RTMATH_RAD_TO_DEGREE;

	if(rover.fusz > 0){
		rover.Compass_Heading = rover.fusz-180;
	}
	else{
		rover.Compass_Heading = rover.fusz+180;
	}
	
	//Datos de Altitud
	
	if(RTMath::convertPressureToHeight(g_imuData.pressure) < 20000)	
	       	rover.rawheight = RTMath::convertPressureToHeight(g_imuData.pressure);
	
	if (rover.rawheight != 0.0 ){
		
		//Insertando el dato de altitud en un arreglo para aplicar un
		//filtro circular de media movil
		MediatorInsert(median,(int) rover.rawheight);
		
		//SET valor de altura referencia del suelo
		
		if (ground_level_ref == 0){
			ground_level_ref = rover.rawheight;
			lim_topalt = ground_level_ref + altura_lanzamiento;
			//printf("ground_level_ref = %f\n", ground_level_ref);
		}
	}
    
    rover.height = MediatorMedian(median);
    
    return 0;
}

int ROVER_OFF(){
	
	set_state(EXITING);
	usleep(900000); // dejando que los hilos terminen
	closeSerial();
	disable_motors();
	
	printf("---------\nROVER OFF\n---------\n");
	
	return 0;	
}

void* bluetooth_remote_control(void* ptr){
	
	//////Función para manejo del rover via control bluetooth app smartphone
	
	char BTRC_buffer[255];
	
	//Para aplicacion Wireless Controller
	/*int radio, angulo;
	char tmp[3]={0};
	float MD, MI;*/
	
	printf("ROVER Bluetooth Remote Controller !!!\n\n");
	
	while (get_state() != EXITING){
		
		XBEE_SerialRead(BTRC_buffer);
		
		//APLICACION ARDUINO BT JOYSTICK
		if(BTRC_buffer[3] == GAS){
			
			switch(BTRC_buffer[4]){
				
				case '#':
					rover.PWMLeft = 1;
					rover.PWMRight = 1;
					
					break;
					
				case LEFT:
					if(rover.PWMLeft > 0)
						rover.PWMLeft -= 0.05;
					else
						rover.PWMLeft = 0;
					break;
					
				case RIGHT:
					if(rover.PWMRight > 0)
						rover.PWMRight -= 0.05;
					else
						rover.PWMRight = 0;
					break;
				
				default:
					break;
			}			
		}
		
		if(BTRC_buffer[3] == BRAKE){
			rover.PWMLeft = 0;
			rover.PWMRight = 0;
		}
		
		//APLICACION WIRELESS CONTROLLER
		/*
		tmp[0]=BTRC_buffer[16];
		tmp[1]=BTRC_buffer[17];
		tmp[2]=BTRC_buffer[18];
		angulo = atoi(tmp);
		tmp[0]=BTRC_buffer[20];
		tmp[1]=BTRC_buffer[21];
		tmp[2]=BTRC_buffer[22];
		radio = atoi(tmp);
		
		if ((0 <= angulo) && (angulo <= 90)){
			MI = (radio - radio*angulo/100.00)/100.00;
			MD = (radio)/100.00;
		}
		
		else if ((270 <= angulo) && (angulo <= 359)){
			MI = (radio)/100.00;
			MD = (radio - radio*(360-angulo)/100.00)/100.00;
		}
		
		else{
			MI = 0.0;
			MD = 0.0;
		}
		
		rover.PWMLeft = MI;
		rover.PWMRight = MD;*/
		
		usleep(150000);
	}
	
	printf("Saliendo Hilo Control Remoto Via Bluetooth\n");
	return 0;
}

void* GPS_monitor(void* ptr){
	
	char GPSstr[255];
    int n;
	
	while(get_state()!=EXITING){
		//Hilo del GPS
		n = GPS_SerialRead(GPSstr);
		if(n){
			getGPS(GPSstr, n);
		}
	}
	
	printf("Saliendo Hilo GPS\n");	
	return 0;
}

void* IMU_monitor(void* ptr){
    //Inicializa struct configuraciones
    RTIMUSettings *settings = new RTIMUSettings("RTIMULib");

    //Creación de struct con información del IMU
    RTIMU *imu = RTIMU::createIMU(settings);

    //Creación de struct con información de la presión
    RTPressure *pressure = RTPressure::createPressure(settings);

    if ((imu == NULL) || (imu->IMUType() == RTIMU_TYPE_NULL)) {
        printf("No IMU found\n");
        return 0;
    }

    //  Inicializa variables del IMU
    imu->IMUInit();
    imu->setSlerpPower(0.02);
    imu->setGyroEnable(true);
    imu->setAccelEnable(true);
    imu->setCompassEnable(true);
    
    pressure->pressureInit();

    while(get_state() != EXITING){
        usleep(imu->IMUGetPollInterval() * 1000);

        while (imu->IMURead()) {
            g_imuData = imu->getIMUData();

            if (pressure != NULL)
                pressure->pressureRead(g_imuData);
        }
    }

    printf("Saliendo Hilo IMU\n");
    
    return 0;
}

int roverNavigation(){
	//funcion para la navegacion de ROVER
	//printf("Error = %f\n", rover.error);
	
	rover.turn = controlPID(rover.error);
			
	//printf("Turn = %f\n", rover.turn);
	
	//Asignacion de PWM para realizar giro
	//Simplemente se pone a girar una llanta al maximo y a la otra se le resta la salida del PID.
	
	if(rover.turn > 0){
		rover.PWMRight = rover.tope_PWM;
		rover.PWMLeft = (rover.tope_PWM - rover.turn);
		
		if(rover.PWMLeft < 0)
			rover.PWMLeft = 0;
	}
	
	else{
		rover.PWMLeft = rover.tope_PWM;
		rover.PWMRight = (rover.tope_PWM + rover.turn);

		if(rover.PWMRight < 0)
			rover.PWMRight = 0;
	}
	
	return 0;
}

void* bluetooth_PID_tunning(void *ptr){
	
	const int check_us = 50000; // dsm2 packets come in at 11ms, check faster
	char buffer[32];
	int i, res;
	int state = WAITING;
	int tempCount = 0;
	int ready = 0;
	char tmp[8]={0};
	char sep1 = ';';
	char sep2 = ',';
	
	float kpScale = 0.004;
	float kiScale = 0.0001;
	float kdScale = 0.1;

	/*char *dataStr = (char *) malloc(255);
	
	FILE *data;
	data = fopen("data.txt", "w");*/
	
	while(get_state()!=EXITING){
		
		//leer la vara serial y modificar la referencia o las constantes de Control
		res = XBEE_SerialRead(buffer);
        if(res){
			for(i = 0; i<res; i++){
							
				if(buffer[i] == sep1){
					
					if(!strcmp(tmp, "RB"))
						state = START;
					
					if(!strcmp(tmp, "RS"))
						state = STOP;

					switch(state){
						case WAITING:
							break;						
						case SP:
							PID.Kp = map(atof(tmp), 0, 20, 0, kpScale);
							break;
						case SI:
							PID.Ki = map(atof(tmp), 0, 20, 0, kiScale);
							break;
						case SD:
	
							PID.Kd = map(atof(tmp), 0, 20, 0, kdScale);
							break;
						case JOYSTICK:
							set_state(EXITING);
							break;
						
						case START:
							PID.resetFLAG = 0;	
							printf("START!!!\n");
							set_state(DEPLOYMENT);
							break;

						case STOP:
							printf("STOP!!!\n");
							set_state(STANDBY);						
							break;

						default:
							break;
					}
					
					state = WAITING;
					memset(&tmp[0], 0, sizeof(tmp));
					tempCount = 0;
				}
				
				else if(buffer[i] == sep2){
					
					if(ready){
						ready = 0;
						state = JOYSTICK;					
					}
					
					if(!strcmp(tmp, "SP"))
						state = SP;
						
					else if(!strcmp(tmp, "SI"))
						state = SI;
						
					else if(!strcmp(tmp, "SD"))
						state = SD;
						
					else if(!strcmp(tmp, "SP"))
						state = SP;
					
					else if(!strcmp(tmp, "CJ")){
						state = JOYSTICK;
						ready = 1;
					}					
						
					memset(&tmp[0], 0, sizeof(tmp));
					tempCount = 0;
				}
				
				else{
					tmp[tempCount] = buffer[i];
					tempCount++;
				}
			}
		}

		//Guardar en un archivo los datos del Compass y course
		//sprintf(dataStr, "%3.2f,%3.2f,%f\n", (double)rover.Compass_Heading, (double) rover.error, atof(gps.Course));
		
		//fprintf(data, dataStr);
		usleep(check_us); 
	}

	//fclose(data);
	
	printf("Cerrando Hilo PID Tunning\n");
	
	return 0;
}

void* send_Serial(void* ptr){
	
	const int send_us = 50000; // enviar datos cada 40ms, si se mandan muy rapido la aplicacion se cae
	char * str = (char *) malloc(255);
	
	while(get_state()!=EXITING){

		if(gps.Fix != 0){
				
			sprintf(str, "E%3.2f,%3.2f,%3.2f\n", (double)rover.height, (double) rover.error, (double)rover.azimuth1_2Goal);
			XBEE_SerialWrite(str); 
		}

		usleep(send_us);
	}
	
	printf("Saliendo Hilo Debug Bluetooth Serial\n");
	
	return 0;
}

void ctrl_c(int signo){
	if (signo == SIGINT){
		set_state(EXITING);
		printf("\nCtrl-C - Saliendo\n");
 	}
}

void parse_args(int argc, char** argv) {
    int ch;
    
    //flag i for no interrupt, v for no verbose
    while((ch=getopt(argc, argv, "drmch?")) != -1) {
        switch(ch) {
            case 'd': ROVER_flags[DEBUG_BLUETOOTH]=1; break;
            case 'r': ROVER_flags[CONTROL_REMOTO]=1; break;
            case 'm': ROVER_flags[MOTORES_DESACTIVADOS]=1; break;
            case 'c': ROVER_flags[CALIBRACION]=1; break;
            case 'h':
            case '?': ROVER_flags[AYUDA]=1; break;
        }
    }
}

void print_usage() {
    printf("\nVENTURER:\n\n");
    printf("Uso: ejecutable [-d] [-r] [-m] [-cal] [-h, -?]\n\n");
    printf("Argumentos:\n");
    printf("-d\tDebug por medio de bluetooth\n");
    printf("-r\tControl remoto con aplicación\n");
    printf("-m\tCon motores desactivados\n");
    printf("-c\tModo de calibración\n"); //El modo de calibración va a guardar los datos obtenidos en un archivo para que no se deba calibrar cada vez que se ejecuta el programa
    printf("-h, -?\tImprimir esto, luego salir\n");
}

float map(float x, float in_min, float in_max, float out_min, float out_max){
	//funcion para escalar valores, tipo regla de 3
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;

}

