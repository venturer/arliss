/* ROVER.h
 * 
 * 
 * 
 * Modifications made by Roy Araya, Mechatronics Engineering Student
 * royaraya16@gmail.com
 * 
 * Based on James Strawson's Robotics Cape code.
 * 
	
Copyright (c) 2015, Roy Araya
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
*/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>		// capture ctrl-c

#include <getopt.h>
#include <linux/input.h>
#include <pthread.h>
#include <poll.h>
#include <time.h>
#include <math.h>
#include <time.h>
#include "RTIMULib.h"

extern "C" {
	#include "GPIO/GPIO.h"
	#include "PWM/PWM.h"
	#include "ADC/ADC.h"
	#include "eQEP/eQEP.h"
	#include "IMU/IMU.h"
	#include "SERIAL/SERIAL.h"
	#include "SERIAL/BTRC_SERIAL.h"
	#include "STATES/STATES.h"
	#include "PID/PID.h"
	#include "GPS/GPS.h"
	#include "FILTRO_FIR/FILTRO_FIR.h"
}

// atan2 and fabs

#define LEFT '3'
#define RIGHT '4'
#define GAS '9'
#define BRAKE '0'

 typedef enum flags_t {
	DEBUG_BLUETOOTH,
	CONTROL_REMOTO,
	MOTORES_DESACTIVADOS,
	CALIBRACION,
	AYUDA
} flags_t;

 typedef enum controlPID_t {
	WAITING,
	SP,
	SI,
	SD,
	JOYSTICK,
	START,
	STOP
} controlPID_t;


typedef struct {
	
	double latGoal;
	double lonGoal;
	
	float PWMLeft;
	float PWMRight;
	
	double distance2Goal;
	double azimuth1_2Goal;
	double azimuth2_2Goal;
	
	double Compass_Heading;
	float GPS_Heading;
	double GPS_Altitude;
		
	// Acelerómetro
	double accx;
	double accy;
	double accz;
	double acelmag;
	double last_acelmag;

	double ac_delta;
	// Fusion
	double fusx;
	double fusy;
	double fusz;

	// Barómetro
	int height;
	double rawheight;

	//variables para el control
	
	float error;
	float turn;
	float tope_PWM;

	//Contadores
	int arriveCount;
	int stuckCount;
	int deployCount;
	
} rover_t;

RTIMU_DATA g_imuData;

rover_t rover;

//arreglos para usar las estructuras
int ROVER_flags[5]; //usado en los flags que se mandan al iniciar el programa

//Constante de espera en el main
const int wait_us = 50000; // dsm2 packets come in at 11ms, check faster


//Parametros de referencia de altura y aceleracion

//referencia del valor de altura del suelo respecto al mar. Punto donde parte y aterriza el ROVER
double ground_level_ref = 0.0;  

//Valor de altura máxima esperada en el lanzamiento.
double lim_topalt = 0.0;

//variable de ajuste que permite definir el limite inferior minimo de altura  esperada según cada lanzamiento
double altura_lanzamiento = 2.0;

//Rango de aceleracion minima para establecer que el rover no se esta moviendo
double maxaceldelta_rep = 0.25; 

//Rango de aceleración maxima durante manipulación normal del rover (ligeros movimientos. ej: al levantarlo y colocar paracaidas)
double maxdelta_normacel = 1.8;

//Variable para enviar vía bluetooth el estado de las etapas del ROVER
char * STATE_str = (char *) malloc(255);

//Estructura para el Filtro FIR
Mediator* median;

int ROVER_ON();

void ctrl_c(int signo); // signal catcher

int ROVER_OFF();

void parse_args(int argc, char** argv);
void print_usage();

float map(float x, float in_min, float in_max, float out_min, float out_max);
int roverNavigation();

void update_Rover();
int update_GPS();
int update_IMU();

void* GPS_monitor(void* ptr);
void* IMU_monitor(void* ptr);
void* bluetooth_remote_control(void* ptr);
void* bluetooth_PID_tunning(void *ptr);
void* send_Serial(void* ptr);



