#include <stdio.h>
#include "geodesic.h"
#include <string.h>
#include <stdlib.h>
#include "SERIAL/SERIAL.h"

//Centro Cancha Volleyball de Playa

#define PLAYA_LATITUDE 9.856941
#define PLAYA_LONGITUDE -83.909164

//Marco Este Cancha Futbol

#define CANCHA_ESTE_LATITUDE 9.856061
#define CANCHA_ESTE_LONGITUDE 83.908992

//Marco Oeste Cancha Futbol

#define CANCHA_OESTE_LATITUDE 9.856139
#define CANCHA_OESTE_LONGITUDE -83.909914

//Tropika

#define TROPIKA_LATITUDE 9.857702
#define TROPIKA_LONGITUDE -83.912433

//Home de Cancha de Beisbol

#define BEISBOL_LATITUDE 9.856212
#define BEISBOL_LONGITUDE -83.907925

//Poste Luz
#define POSTE_LATITUDE 9.856873
#define POSTE_LONGITUDE -83.913786

//Detras Labs Fisica
#define LAB_LATITUDE 9.854828
#define LAB_LONGITUDE -83.913164

#define PARADA_LATITUDE 9.855999
#define PARADA_LONGITUDE -83.913642

enum gpsValue_t{
	ID,
	UTC,
	LATITUDE,
	N_S,
	LONGITUDE,
	E_W,
	FIX,
}gpsValue_t;

typedef struct{
	
	char *GGA_ID;
	char *utc;
	char *latitudeDMS;
	char *n_s;
	char *longitudeDMS;
	char *e_w;
	char *charFix;
	char *Satellites;
	char *HDOP;
	char *Altitude;
	
	char *VTG_ID;
	char *Course;
	char *Speed;
	
	double latitudeDD;
	double longitudeDD;
	
	int Fix;
	
} gps_t;

gps_t gps;
struct geod_geodesic g;

int init_GPS();
double get_Distance();
int getGPS(char *str, int length);
char *substring(char *string, int position, int length);
