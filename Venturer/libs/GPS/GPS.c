# include "GPS/GPS.h"

int init_GPS(){
	
	double a = 6378137, f = 1/298.257223563;
	geod_init(&g, a, f);

	gps.GGA_ID = (char *) malloc(128);
	gps.utc = (char *) malloc(128);
	gps.latitudeDMS = (char *) malloc(128);
	gps.n_s = (char *) malloc(128);
	gps.longitudeDMS = (char *) malloc(128);
	gps.e_w = (char *) malloc(128);
	gps.charFix = (char *) malloc(128);
	gps.Satellites = (char *) malloc(128);
	gps.HDOP = (char *) malloc(128);
	gps.Altitude = (char *) malloc(128);
	
	gps.VTG_ID = (char *) malloc(128);
	gps.Course = (char *) malloc(128);
	gps.Speed = (char *) malloc(128);
	
	strcpy(gps.latitudeDMS, "0");
	strcpy(gps.charFix, "0");
	
	strcpy(gps.Course, "0");
	strcpy(gps.Speed, "0");

	GPS_SerialWrite("$PMTK314,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"); //solo envie GGA y VTG

	GPS_SerialWrite("$PMTK220,200*2C\r\n"); // 5 Hz

	GPS_SerialWrite("$PMTK313,1*2E\r\n"); // Enable to search a SBAS satellite

	GPS_SerialWrite("$PMTK301,2*2E\r\n"); // Enable WAAS as DGPS Source
	
	
	return 0;
}

double get_Distance(){
	
	double azi1, azi2, s12;
	
	geod_inverse(&g, TROPIKA_LATITUDE, TROPIKA_LONGITUDE, PLAYA_LATITUDE, PLAYA_LONGITUDE, &s12, &azi1, &azi2);

	printf("Distancia= %f \t Azimuth_1= %f \t Azimuth_2= %f \n", s12, azi1, azi2);
	return s12;
}

int getGPS(char *str, int length){
	
	int i;
	char * pch;
	pch = (char *) malloc(128);
	pch = strtok (str,",");
    
	//Obtencion de Global Positioning System Fixed Data
	//if((!strcmp(pch, "$GPGGA") && length > 70) || (!strcmp(pch, "$GPVTG") && length > 37)){
		
	//printf("---FIX---\n");
	
	if(!strcmp(pch, "$GPGGA")){
		
		for(i=0;i<9;i++){
			
			pch = strtok (NULL, ",");
			
			switch(i){
				case 0:	
					pch != NULL ? strcpy(gps.utc, pch) : strcpy(gps.utc, "0.0");	
					break;
				case 1:	
					pch != NULL ? strcpy(gps.latitudeDMS, pch) : strcpy(gps.latitudeDMS, "0.0");
					break;
				case 2:	
					strcpy(gps.n_s, pch);	
					pch != NULL ? strcpy(gps.n_s, pch) : strcpy(gps.n_s, "0.0");
					break;
				case 3:	
					pch != NULL ? strcpy(gps.longitudeDMS, pch) : strcpy(gps.longitudeDMS, "0.0");
					break;
				case 4:	
					pch != NULL ? strcpy(gps.e_w, pch) : strcpy(gps.e_w, "0.0");
					break;
				case 5:	
					pch != NULL ? strcpy(gps.charFix, pch) : strcpy(gps.charFix, "0");
					break;
				case 6:		
					pch != NULL ? strcpy(gps.Satellites, pch) : strcpy(gps.Satellites, "0.0");
					break;
				case 7:	
					pch != NULL ? strcpy(gps.HDOP, pch) : strcpy(gps.HDOP, "0.0");
					break;
				case 8:	
					pch != NULL ? strcpy(gps.Altitude, pch) : strcpy(gps.Altitude, "0.0");
					break;
				default: 
					break;
			}
		}
		
		if(!strcmp(gps.charFix, "0") || !strcmp(gps.latitudeDMS, "0"))
			gps.Fix = 0;			
				
		else
			gps.Fix = 1;
		
		
		if(gps.Fix){		

			//Conversion de Grados,Minutos,Segundos a Grados Decimales
			gps.latitudeDD = atof(substring(gps.latitudeDMS, 1, 2)) + ((atof(substring(gps.latitudeDMS, 3, 7)))/60);

			if(!strcmp(gps.n_s, "S"))
				gps.latitudeDD = -1*gps.latitudeDD;
		
			gps.longitudeDD = atof(substring(gps.longitudeDMS, 1, 3)) + ((atof(substring(gps.longitudeDMS, 4, 7)))/60);
			
			if(!strcmp(gps.e_w, "W"))
				gps.longitudeDD = -1*gps.longitudeDD;
		}
			
	}

	//Obtencion de Course Over Ground and Ground Speed
	else if(!strcmp(pch, "$GPVTG") && gps.Fix){
		for(i=0;i<7;i++){
			
			pch = strtok (NULL, ",");
		
			switch(i){
				case 0:	
					pch != NULL ? strcpy(gps.Course, pch) : strcpy(gps.Course, "0");
					break;
				case 5:	
					pch != NULL ? strcpy(gps.Speed, pch) : strcpy(gps.Speed, "0");
					break;
				default: break;
			}
		}
	}
	
	return 0;
}

char *substring(char *string, int position, int length){
	
   char *pointer;
   int c;
 
   pointer = malloc(length+1);
 
   if (pointer == NULL)
   {
      printf("Unable to allocate memory.\n");
      exit(1);
   }
 
   for (c = 0 ; c < length ; c++)
   {
      *(pointer+c) = *(string+position-1);      
      string++;   
   }
 
   *(pointer+c) = '\0';
 
   return pointer;
}


