#include "SERIAL/BTRC_SERIAL.h"

struct termios uart1,old;

int BTRC_initSerial(char BTRC_uartX_directory[30]){
	
    /* Open modem device for reading and writing and not as controlling tty
       because we don't want to get killed if linenoise sends CTRL-C. */
	int fd = open(BTRC_uartX_directory, O_RDWR | O_NOCTTY);
	if(fd < 0) {printf("port failed to open\n");}

	//save current attributes
	tcgetattr(fd,&old);
	bzero(&uart1,sizeof(uart1)); /* clear struct for new port settings */

	/* BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
       CRTSCTS : output hardware flow control (only used if the cable has
                 all necessary lines. See sect. 7 of Serial-HOWTO)
       CS8     : 8n1 (8bit,no parity,1 stopbit)
       CLOCAL  : local connection, no modem contol
       CREAD   : enable receiving characters */


	uart1.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
	uart1.c_iflag = IGNPAR | ICRNL;  //IGNPAR  : ignore bytes with parity errors
	uart1.c_oflag = 0;
	uart1.c_lflag = ICANON;
	uart1.c_cc[VTIME] = 0;
	uart1.c_cc[VMIN]  = 1;

	//clean the line and set the attributes
	tcflush(fd,TCIFLUSH);
	tcsetattr(fd,TCSANOW,&uart1);
	//fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK); 
	close(fd);
	//fd = open(uartX_directory, O_RDWR | O_NOCTTY);
	
	return 0;
}

int BTRC_SerialWrite(char BTRC_uartX_directory[30],char message[]){
	int portdir = open(BTRC_uartX_directory, O_RDWR | O_NOCTTY);
	write(portdir, message, strlen(message));
	close(portdir);
	return 0;
}

int BTRC_SerialRead(int open_portdir,char buffer[255]){
   	int n = read(open_portdir, buffer, 255);// recuerde enviar al final del string "\n", en el mensaje que esta leyendo por el puerto serie
	if (n < 0){
       	fputs("read failed!\n", stderr);
	}
	else{
		//printf("%s",buffer);
		}
	return 0;
}

