#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define GPS_DIR "/dev/ttyO1"
#define XBEE_DIR "/dev/ttyO2"

int initGPSSerial();
int initBLUETOOTH_Serial();
int GPS_SerialWrite(char message[]);
int GPS_SerialRead(char buffer[255]);
int XBEE_SerialRead(char buffer[255]);
int XBEE_SerialWrite(char *message);
int closeSerial();
