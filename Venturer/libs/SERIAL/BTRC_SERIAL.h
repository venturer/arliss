#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int BTRC_initSerial(char uartX_directory[30]);
int BTRC_SerialWrite(char uartX_directory[30],char message[]);
int BTRC_SerialRead(int open_portdir,char buffer[255]); 
