#include "SERIAL/SERIAL.h"

struct termios uart1,old,uart2,old2;
int fd,fd2,res,res2;

int initGPSSerial(){
	
    /* Open modem device for reading and writing and not as controlling tty
       because we don't want to get killed if linenoise sends CTRL-C. */
	fd = open(GPS_DIR, O_RDWR | O_NOCTTY);
	if(fd < 0) {printf("port failed to open\n");}

	//save current attributes
	tcgetattr(fd,&old);
	bzero(&uart1,sizeof(uart1)); /* clear struct for new port settings */

	/* BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
       CRTSCTS : output hardware flow control (only used if the cable has
                 all necessary lines. See sect. 7 of Serial-HOWTO)
       CS8     : 8n1 (8bit,no parity,1 stopbit)
       CLOCAL  : local connection, no modem contol
       CREAD   : enable receiving characters */


	uart1.c_cflag = B57600 | CS8 | CLOCAL | CREAD;
	uart1.c_iflag = IGNPAR | ICRNL;  //IGNPAR  : ignore bytes with parity errors
	uart1.c_oflag = 0;
	uart1.c_lflag = ICANON;

	//clean the line and set the attributes
	tcflush(fd,TCIFLUSH);
	tcsetattr(fd,TCSANOW,&uart1);
	return 0;
}

int initBLUETOOTH_Serial(){
	
    /* Open modem device for reading and writing and not as controlling tty
       because we don't want to get killed if linenoise sends CTRL-C. */
	fd2 = open(XBEE_DIR, O_RDWR | O_NOCTTY);
	if(fd2 < 0) {printf("port failed to open\n");}

	//save current attributes
	tcgetattr(fd2,&old2);
	bzero(&uart2,sizeof(uart2)); /* clear struct for new port settings */

	/* BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
       CRTSCTS : output hardware flow control (only used if the cable has
                 all necessary lines. See sect. 7 of Serial-HOWTO)
       CS8     : 8n1 (8bit,no parity,1 stopbit)
       CLOCAL  : local connection, no modem contol
       CREAD   : enable receiving characters */


	uart2.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
	uart2.c_iflag = IGNPAR | ICRNL;  //IGNPAR  : ignore bytes with parity errors
	uart2.c_oflag = 0;
	//uart2.c_lflag = ~ICANON;
	//uart2.c_cc[VTIME]=0;
	//uart2.c_cc[VMIN] =1;

	//clean the line and set the attributes
	tcflush(fd2,TCIFLUSH);
	tcsetattr(fd2,TCSANOW,&uart2);
	return 0;
}

int GPS_SerialWrite(char *message){

	write(fd, message, strlen(message));
	return 0;
}

int GPS_SerialRead(char buffer[255]){
	
	int n;
   	n = read(fd, buffer, 255);
   	buffer[n] = '\0';
   	
	if (n < 2){ // ver bien porque en una 'n' vale 38 y en la otra 1
       	return 0;
	}
		
	return n;
}

int XBEE_SerialRead(char buffer[255]){
	
	int n;
   	n = read(fd2, buffer, 255);
   	buffer[n] = '\0';		
	return n;
}

int XBEE_SerialWrite(char * message){
		
	write(fd2, message, strlen(message));
	return 0;
}

int closeSerial(){
	printf("Cerrando Serial\n");
	close(fd);
	close(fd2);
	return 0;	
}

