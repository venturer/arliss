

#include "PID/PID.h"
#include "STATES/STATES.h"

int setPID(float Kp, float Ki, float Kd){
	
	PID.Kp = Kp;
	PID.Ki = Ki;
	PID.Kd = Kd;
	
	return 0;
}

int reset_PID(){
	printf("RESET\n");
	PID.resetFLAG = 0;
	PID.integral = 0;
	PID.last_error = 0;
	
	return 0;
}

float controlPID(float error){
	
	PID.error = error;
			
	PID.proporcional = PID.error * PID.Kp;
	PID.integral = PID.integral + PID.error * PID.Ki;
	PID.diferencial = (PID.error - PID.last_error) * PID.Kd;
	
	//printf("Error = %f \t Ki = %f \n", PID.error, PID.Ki);
	//printf("Proporcional = %f \t Integral = %f \t Diferencial = %f \n", PID.proporcional, PID.integral, PID.diferencial);

	if(PID.integral > 0.5 || PID.integral < -0.5){
		printf("STUCK!\n");
		set_state(STUCK);	
	}

	//Se actualiza el ultimo valor del error para el termino diferencial
	PID.last_error = PID.error;
	
	//Retornando el valor proporcionado por el calculo del PID
	//return (PID.proporcional + PID.integral + PID.diferencial);
	return (PID.proporcional + PID.diferencial);
}
