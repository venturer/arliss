
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
	
	float Kp;
	float Ki;
	float Kd;
	
	float proporcional;
	float integral;
	float diferencial;
	
	float error;
	float last_error;
	
	float out;
	
	int resetFLAG;	
	
} PID_t;

PID_t PID;

int setPID(float Kp, float Ki, float Kd);
int reset_PID();
float controlPID(float error);
