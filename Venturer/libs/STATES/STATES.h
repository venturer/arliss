
#include "stdio.h" 
 
 
 typedef enum state_t {
	STANDBY,
	LAUNCH,
	LANDING,
	DEPLOYMENT,
	NAVIGATION,
	STUCK,
	EXITING
} state_t;

enum state_t get_state();
int set_state(enum state_t);
int change_state();
