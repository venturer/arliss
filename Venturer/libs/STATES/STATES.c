#include "STATES/STATES.h"

enum state_t state = LAUNCH;

enum state_t get_state(){
	return state;
}

int set_state(enum state_t new_state){
	state = new_state;
	return 0;
}

int change_state(){
	/*
	switch(get_state()){
		
		case LAUNCH:
		     //Se verifica si se encuentra en la etapa de lanzamiento, al detectar el ascenso y/o descenso con un delta de ACEL y AlT
		     //Si se detecta el lanzamiento, se pasa a la etapa LANDING.
		     if ((rover.acelmag > max_normacel)&&(rover.height > lim_topalt)){
				 printf("---------\nETAPA DE LANZAMIENTO\n---------\n");
				 sprintf(STATE_str, "---------\nETAPA DE LANZAMIENTO\n---------\n");
			     XBEE_SerialWrite(STATE_str);
				 set_state(LANDING);
			 }
		     break;	
		
		case LANDING:
		    //Espera y verifica si el ROVER ya aterrizó. Para ello verifica que la aceleración y altura vuelvan a sus valores de referencia.
		    //Una vez que ha aterrizado, ejecuta la rutina de separacion de paracaidas y pasa al estado NAVIGATION.
		    double min_groundheight = ground_level_ref - 2;//Esto porque la incertidumbre del barómetro es de +- 2 metros
		    double max_groundheight = ground_level_ref + 2;
		    if (((minacel_reposo < rover.acelmag)&&(rover.acelmag < maxacel_reposo))&&((min_groundheight < rover.height)&&(rover.height < max_groundheight))){
				 //RUTINA DE SEPARACIÓN DEL PARACAIDAS
				 rover.PWMLeft = 0.3;
				 rover.PWMRight = 0.3;
				 printf("---------\nETAPA DE ATERRIZAJE CONCLUIDA\n---------\n");
				 sprintf(STATE_str, "---------\nETAPA DE ATERRIZAJE CONCLUIDA\n---------\n");
			     XBEE_SerialWrite(STATE_str);
				 printf("---------\nINICIO DE NAVEGACIÓN\n---------\n");
				 sprintf(STATE_str, "---------\nINICIO DE NAVEGACIÓN\n---------\n");
			     XBEE_SerialWrite(STATE_str);
				 set_state(NAVIGATION);
			 }
			break;
		
		case NAVIGATION:  //FALTA DETECTAR STUCK  y CONDICIÓN DE LLEGADA A LA META !!!!!!!!!!!!!!!!!!!
		    //El Rover comienza a dirigirse hacia la meta.
		    //Se debe verificar si hay atascamiento del ROVER en todo momento, de presentarse se brinca al estado STUCK.
			//Calculo del error para la entrada del control
			rover.error =  rover.azimuth1_2Goal - rover.Compass_Heading;
			//Algoritmo de control	
			roverNavigation();
			break;
		
		case STUCK: //FALTA RUTINA DE DESATASCAMIENTO !!!!!!!!!!!!!!!
			//rutina de desatascamiento
			//set_state(NAVIGATION);
			break;
			
		case EXITING:
			return 0;
			break;
		
		default:
			break;
	}
	*/
	return 0;
}
