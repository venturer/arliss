\select@language {spanish}
\contentsline {chapter}{Agradecimientos}{1}{chapter*.1}
\contentsline {chapter}{Autores}{2}{chapter*.2}
\contentsline {chapter}{\numberline {1}Introducci\'on}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Descripci\'on general}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Estado del arte}{1}{section.1.2}
\contentsline {chapter}{\numberline {2}Planificaci\'on}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Objetivos}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Rol de integrantes}{2}{section.2.2}
\contentsline {section}{\numberline {2.3}An\'alisis modal de fallos y efectos (AMFE)}{3}{section.2.3}
\contentsline {section}{\numberline {2.4}Planeamiento Temporal, diagrama de Gantt}{4}{section.2.4}
\contentsline {chapter}{\numberline {3}Mec\'anica}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Paraca\IeC {\'\i }das}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Mecanismo de liberaci\'on}{7}{section.3.2}
\contentsline {section}{\numberline {3.3}Ruedas}{7}{section.3.3}
\contentsline {section}{\numberline {3.4}Estructura}{7}{section.3.4}
\contentsline {chapter}{\numberline {4}Hardware}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}Microprocesador}{8}{section.4.1}
\contentsline {section}{\numberline {4.2}Telemetr\IeC {\'\i }a}{8}{section.4.2}
\contentsline {section}{\numberline {4.3}Motores}{8}{section.4.3}
\contentsline {section}{\numberline {4.4}Sensores}{8}{section.4.4}
\contentsline {section}{\numberline {4.5}Potencia}{8}{section.4.5}
\contentsline {section}{\numberline {4.6}Conectores y tarjeta impresa}{8}{section.4.6}
\contentsline {chapter}{\numberline {5}Software y control}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}Sistema operativo}{9}{section.5.1}
\contentsline {section}{\numberline {5.2}Detecci\'on a\'erea}{9}{section.5.2}
\contentsline {section}{\numberline {5.3}Navegaci\'on}{9}{section.5.3}
\contentsline {section}{\numberline {5.4}Algoritmo de obstrucci\'on}{9}{section.5.4}
\contentsline {section}{\numberline {5.5}Optimizaci\'on de punto de llegada}{9}{section.5.5}
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{10}{chapter*.6}
\contentsline {chapter}{Ap\'endices}{I}{section*.7}
\contentsline {chapter}{\numberline {A}Este ser\'a un lindo ap\'endice }{II}{appendix.Alph1}
