#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdexcept> 
#include <iostream>
#include <fstream>
#include <fcntl.h>

#include <pstreams/pstream.h>

//Tiempo de espera entre cada muestreo.
#define TIEMPO 1


/**************** Diagrama de Conexiones ********************

BAT->---------- Rin -----------------------------> Step-down
    -                        -
    -                        -
    R11                      R21
    -                        -
    -                        -
    ---P38 AIN3              ----P40 AIN1
    -                        -
    -                        -
    R12                      R22
    -                        -
    -                        -
    GND                      GND      
*/



using namespace std;

// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
string currentDateTime(int format = 0) {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);

    if(format == 1){

        strftime(buf, sizeof(buf), "%d--%X--%m-%Y", &tstruct);
    }else{
    
        strftime(buf, sizeof(buf), "%d-%m-%Y %X", &tstruct);
    }
    return buf;
}

void getLecs(float &lec1, float &lec2){

    
    int fuck = 0;
    string temp;
    
    
    redi::ipstream proc("cat /sys/devices/ocp.2/helper.11/AIN3", redi::pstreams::pstdout);

    while (std::getline(proc.out(), temp))
    lec1 = stoi(temp);

    redi::ipstream proc2("cat /sys/devices/ocp.2/helper.11/AIN1", redi::pstreams::pstdout);

    while (std::getline(proc2.out(), temp))
    lec2 = stoi(temp);

    proc.close();
    proc2.close();
    
    
    
/*    
    do{
        try {*/
        
            /* C++
            ifstream pin; 
            pin.open("/sys/devices/ocp.2/helper.11/AIN0", ifstream::in);
            ifstream pin1;
            pin1.open("/sys/devices/ocp.2/helper.11/AIN1", ifstream::in);
            
            if(pin.is_open() && pin1.is_open()){
                //getline( pin, temp);
                pin >> temp;
                lec1 = stoi(temp);
                
                getline( pin1, temp);
                lec2 = stoi(temp);
                
                pin.close();
                pin1.close();
            
            }else{
            
                cout << "No se abrio el cerote" << endl;
                fuck = 1;
            
            }*/
            
            
            /* C
            char buf[20];
            
            int fd = open("/sys/devices/ocp.2/helper.11/AIN0", O_RDONLY |O_NONBLOCK );
            int fd2 = open("/sys/devices/ocp.2/helper.11/AIN1", O_RDONLY |O_NONBLOCK );
            
            int caract = read(fd, buf, 20);
            temp = string(buf, buf+caract);
            lec1 = stoi(temp);
            
            caract = read(fd2, buf, 20);
            temp = string(buf, buf+caract);
            lec2 = stoi(temp);
            
            close(fd);
            close(fd2);*/
            
/*  
        }
        catch (const std::invalid_argument& lol) {
            fuck = 1;
            cout << "Fallo stoi: " << temp << " -- Dice que: "<< lol.what() <<  endl;
            sleep(10);
        
        }
    }while(fuck == 1);*/
    
    
}

int main()
{
    float Rin = 1.5;
    float R11 =  3960;
    float R12 = 1001;
    float R21 = 4001;
    float R22 = 1000;
    float Vteo = 8.4;
    float Voteo1 = (R12/(R12+R11))*Vteo;
    float Voteo2 = (R22/(R22+R21))*Vteo;
    
    float lec1, lec2, diff, real, realLec1, realLec2, current;

    string date = currentDateTime(1);
    string filename = "";
    filename += "./Log/" + date +".txt";   

    ofstream file;
    file.open (filename.c_str(), ios::out | ios::app);
    
    /* Date: Fecha en que fue tomada la muestra.
       Hour: Hora en que fue tomada la muestra.
       Lec1: Lectura de la tensión en el punto 1.
       Lec2: Lectura de la tensión en el punto 2.
       Real: Valor real de Diff usando los valores fisicos de las resistencias.
       RealLec1: Valor real de Lec1 (Tensión de la Bateria de control) en mV.
       RealLec2: Valor real de Lec2 (Tensión de la Bateria de control) en mV. 
       CurrentmA: Corriente en mA demandada por el sistema.
   */
    
    file << "Date Hour Lec1 Lec2  RealLec1mV RealLec2mV RealmV CurrentmA" << endl;
    
    
    do{
        sleep(TIEMPO);
        
        getLecs(lec1, lec2);
        
        diff = lec1 - lec2;
        realLec1 = (lec1*Vteo)/Voteo1;
        realLec2 = (lec2*Vteo)/Voteo2;
        real = realLec1 - realLec2;
        current = real/Rin;
        
        file << currentDateTime(0) << " " << lec1 << " "  << lec2 << " " << realLec1 << " " << realLec2 << " " << real << " " << current << endl;
         

    }while(realLec1 > 6400);
    
    
    
    
    
    file.close();

    
    //setuid( 0 );
    //system( "~/startALL.sh" );

   return 0;
}
