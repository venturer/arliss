#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <iostream>

using namespace std;

int main(){


    int fd; /* port file descriptor */
    char port[20] = "/dev/ttyO4"; /* port to connect to */
    speed_t baud = B9600; /* baud rate */

    fd = open(port, O_RDWR); /* connect to port */

    /* set the other settings (in this case, 9600 8N1) */
    struct termios settings;
    tcgetattr(fd, &settings);

    cfsetospeed(&settings, baud); /* baud rate */
    settings.c_cflag &= ~PARENB; /* no parity */
    settings.c_cflag &= ~CSTOPB; /* 1 stop bit */
    settings.c_cflag &= ~CSIZE;
    settings.c_cflag |= CS8 | CLOCAL; /* 8 bits */
    settings.c_lflag = ICANON; /* canonical mode */
    settings.c_oflag &= ~OPOST; /* raw output */

    tcsetattr(fd, TCSANOW, &settings); /* apply the settings */
    tcflush(fd, TCOFLUSH);

    /* — code to use the port here — */
    
    string code;
    
    int mask = 0x7F;
    int check = 255;
    code = (char)(mask&check);
    
    char send;
     
    send = 128;

    
    write(fd, &send, 1);

    send = 8;
    write(fd, &send, 1);

    send = 64;
    write(fd, &send, 1);
    
    send = 200&mask;
    write(fd, &send, 1);


    close(fd); /* cleanup */

    return 0;
}

