#include "RoboClaw.h"

using namespace std;
using namespace LibSerial;

#define address 0x80

int main(){

    RoboClaw *roboclaw = new RoboClaw;
    roboclaw->ForwardM1(128, 64);
    roboclaw->ForwardM2(128, 64);
    
  uint8_t status1,status2,status3,status4;
  bool valid1,valid2,valid3,valid4;
  uint16_t temp;
  
  //Read all the data from Roboclaw before displaying on terminal window
  //This prevents the hardware serial interrupt from interfering with
  //reading data using software serial.
  uint32_t enc1= roboclaw->ReadEncM1(address, &status1, &valid1);
  uint32_t enc2 = roboclaw->ReadEncM2(address, &status2, &valid2);
  uint32_t speed1 = roboclaw->ReadSpeedM1(address, &status3, &valid3);
  uint32_t speed2 = roboclaw->ReadSpeedM2(address, &status4, &valid4);
  roboclaw->ReadTemp(address,temp);
  
  cout << temp/10 << endl;
  cout << enc2 << endl;
  cout << speed1 << endl;
  cout << speed2 << endl;
/*
    RoboClaw robo;
   bool lol =  ForwardM1(128,64);*/

    return 0;
}
