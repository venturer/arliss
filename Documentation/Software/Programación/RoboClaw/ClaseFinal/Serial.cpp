#include "Serial.h"


using namespace std;
using namespace LibSerial;


Serial::Serial(){

    port.Open( "/dev/ttyO4" ) ;
    port.SetBaudRate( SerialStreamBuf::BAUD_9600 ) ;
    port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8 ) ;
    port.SetNumOfStopBits(1) ;
    port.SetParity( SerialStreamBuf::PARITY_NONE ) ;


}

//
// Destructor
//
Serial::~Serial(){
}

bool Serial::writeS(int value){

    temp = value;
    port << temp;
    
    return true;

}

char Serial::readS(){

    port >> temp;
    
    return temp;
}

/*
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <iostream>
#include <cstdlib>
#include <sys/select.h>
#include <SerialStream.h>
#include <sstream>
#include <stdarg.h>

using namespace std;
using namespace LibSerial ;

int main(){

    SerialStream my_serial_stream ;

    my_serial_stream.Open( "/dev/ttyO4" ) ;
    my_serial_stream.SetBaudRate( SerialStreamBuf::BAUD_9600 ) ;
    my_serial_stream.SetCharSize( SerialStreamBuf::CHAR_SIZE_8 ) ;
    my_serial_stream.SetNumOfStopBits(1) ;
    my_serial_stream.SetParity( SerialStreamBuf::PARITY_NONE ) ;
    
    
    char send;
     
    send = 128;
    my_serial_stream << send;
    
    send = 82;
    my_serial_stream << send;
    
    
    uint16_t value;
    
    uint8_t data;
    my_serial_stream >> data ;
    
    value=(uint16_t)data<<8;
    
    my_serial_stream >> data ;
    
    value|=(uint16_t)data;
    
    cout << value << endl;
    
    /*
    char next_char ;
    
    const int BUFFER_SIZE = 3 ;
    char buffer[BUFFER_SIZE] ; 
    //
    my_serial_stream.read( buffer, 
                           BUFFER_SIZE ) ;
                           
    stringstream ss;
    signed short int tempAngle;

	  //-- Show the received data
    for(int ii=0; ii < 2; ii++) {
    int lol = buffer[ii];
    
    cout << lol << endl;
      ss << buffer[ii];
    }

    ss >> tempAngle;
    
    cout << tempAngle << endl;*/
    
    /*
    my_serial_stream >> next_char ;
    
    cout << next_char << endl;
    
    my_serial_stream >> next_char ;
    
    cout << next_char << endl;
    
    my_serial_stream >> next_char ;
    */

/*   return 0;
}*/
