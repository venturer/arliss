#!/bin/bash

cape="bone_capemgr.8"
FILE=/dev/ttyO4

 
if [ -e $FILE ];
then
   echo "Buenas... No es el primer inicio"
else
   #Aqui van todos los pines que hay que montar
   echo ttyO4_armhf.com > /sys/devices/${cape}/slots
   echo ttyO1_armhf.com > /sys/devices/${cape}/slots
   echo ttyO2_armhf.com > /sys/devices/${cape}/slots
   echo cape-bone-iio > /sys/devices/${cape}/slots
   stty ispeed 57600 ospeed 57600 -F /dev/ttyO4

   #Poner aqui los archivos a ejecutar
   #/home/ubuntu/DataConsumo/elec
   #/home/ubuntu/Robo/grande
   #/home/ubuntu/principal/main
   #/home/ubuntu/fall/fall
fi
