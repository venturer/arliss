#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <ctime>
#include <math.h>
#include "L3G.h"
#include "LPS331.h"
#include "LSM303.h"
#include <chrono>

#define ACC_SC_X_NEG 0.00972222f
#define ACC_SC_X_POS 0.00921053f
#define ACC_SC_Y_NEG 0.00964567f
#define ACC_SC_Y_POS 0.00968379f
#define ACC_SC_Z_NEG 0.00995435f
#define ACC_SC_Z_POS 0.00894161f

#define M_GNxy 1100 // LSB/gauss
#define M_GNz 980  // LSB/gauss

#define ToRad(x) ((x) * 0.01745329252)  // *pi/180
#define ToDeg(x) ((x) * 57.2957795131)  // *180/pi

#define SECS_TO_START 1800				// Segundos de espera para guardan robot
#define SECS_TO_DEPLOY 5400				// Segundos para empezar
#define SECS_AFTER_LAUNCH 1200			// Segundos para empezar, una vez detectado despegue
#define LAUNCH_ACCELERATION 30  		// m/s2
#define DEPLOY_ACCELERATION 120			// m/s2

using namespace std;

L3G gyro;
LPS331 baro;
LSM303 compass;

float gyro_x, gyro_y, gyro_z;
int16_t accel_x,accel_y,accel_z;
float smoothAccX,smoothAccY,smoothAccZ;
float accToFilterX,accToFilterY,accToFilterZ;
float pressure, altitude;
float acc_magnitude, gyro_magnitude;
vector<float> altitude_array, gyro_array, acc_array;
int ascend_count = 0;
int descend_count = 0;
int altitude_count = 0;


void Smoothing(int16_t *raw, float *smooth){
	*smooth = (*raw * (0.15)) + (*smooth * 0.85);
}

void Read_Gyro()
{
  gyro.read();
  
  gyro_x = gyro.g.x * ToRad(-0.07);
  gyro_y = gyro.g.y * ToRad(-0.07);
  gyro_z = gyro.g.z * ToRad(-0.07);
  
	gyro_magnitude = sqrt(pow(gyro_x,2)+pow(gyro_y,2)+pow(gyro_z,2));
  
}

void Read_Accel()
{
  compass.readAcc();
 
  accel_x = compass.a.x >> 4;
  accel_x *= -1.0;
  Smoothing(&accel_x,&smoothAccX);//this is a very simple low pass digital filter
  accel_y = compass.a.y >> 4;
  accel_y *= -1.0; //* -1.0;
  Smoothing(&accel_y,&smoothAccY);//it helps significiantlly with vibrations. 
  accel_z = compass.a.z >> 4;
  accel_z *= -1.0; //* -1.0;
  Smoothing(&accel_z,&smoothAccZ);//if the applicaion is not prone to vibrations this can skipped and the raw value simply recast as a float
 
  if (smoothAccX > 0){
    accToFilterX = smoothAccX * ACC_SC_X_POS;
  }
  else{
    accToFilterX = smoothAccX * ACC_SC_X_NEG;
  }
  if (smoothAccY > 0){
    accToFilterY = smoothAccY * ACC_SC_Y_POS;
  }
  else{
    accToFilterY = smoothAccY * ACC_SC_Y_NEG;
  }
  if (smoothAccZ > 0){
    accToFilterZ = smoothAccZ * ACC_SC_Z_POS;
  }
  else{
    accToFilterZ = smoothAccZ * ACC_SC_Z_NEG;
  }   

	acc_magnitude = sqrt(pow(accToFilterX,2)+pow(accToFilterY,2)+pow(accToFilterZ,2));
}

void Read_Barometer(){
	pressure = baro.readPressureMillibars();
	altitude = baro.pressureToAltitudeMeters(pressure);	
}

void Baro_Init(){
	baro.init();
	baro.enableDefault();

	for(int i = 0; i<500; i++){
		Read_Barometer();	
	 	usleep(5);
	}
}

void Accel_Init()
{
  compass.init();
  compass.enableDefault();
  
  for(int j = 0; j < 300; j++){
    Read_Accel();
    usleep(5);
  }
}

void Gyro_Init()
{
  gyro.init();
  gyro.enableDefault();
  
  gyro.writeReg(L3G_CTRL_REG4, 0x20);
  gyro.writeReg(L3G_CTRL_REG1, 0x0F); 
}

void split(string &s, char delim, vector<float> &elems){
	stringstream ss(s);
    string item;
	elems.clear();
    while(getline(ss, item, delim)) {
        elems.push_back(atof(item.c_str()));
    }
}

int impact_acc(){
	int first_half=0;
	int first_half_low=0;
	float avg_acc0 = 0.0;
	float max_acc = 0.0;
	for(unsigned int i=70; i<150; i++){
		if(acc_array[i] > 15) first_half++; 
		if(acc_array[i] < 8) first_half_low++;
		avg_acc0 += acc_array[i];
		if(acc_array[i] > max_acc) max_acc = acc_array[i];
	}

	if(max_acc < 20.0) return 0;
	
	avg_acc0 = avg_acc0/80;

	int second_half=0;
	int second_half_low=0;
	float avg_acc1 = 0.0;
	for(unsigned int i=0; i<acc_array.size(); i++){
		if(i<70 || i>150){
			if(acc_array[i] > 11) second_half++; 
			if(acc_array[i] < 8) second_half_low++;
			avg_acc1 += acc_array[i];
		}	
	}
	avg_acc1 = avg_acc1/(acc_array.size()-80);

	if(first_half > 7 && first_half < 20 && second_half << 5) {
		float var0 = 0.0;
		for(unsigned int i=70; i<150; i++){
			var0 += pow((acc_array[i]-avg_acc0),2);
		}
		var0 = var0/80;

		float var1 = 0.0;
		for(unsigned int i=0; i<acc_array.size(); i++){
			if(i<70 || i>150){
				var1 += pow((acc_array[i]-avg_acc1),2);
			}	
		}
		var1 = var1/(acc_array.size()-80);
		cout << var0 << " " << var1 << endl;
		
		if(var0 > 15.0 && var1 < 5.5)return 1;
		else return 0;
	}
	else return 0;
}

int impact(){
	if(impact_acc()){
		return 1;
	}
	else{
	 	return 0;
	}
}

int is_still_altitude(){
	float avg_altitude = 0.0;
	for(unsigned int i=0; i<altitude_array.size(); i++){
		avg_altitude += altitude_array[i];	
	}
	avg_altitude = avg_altitude/altitude_array.size();
	for(unsigned int i=0; i<altitude_array.size(); i++){
		if(altitude_array[i] > avg_altitude+5.0 || altitude_array[i] < avg_altitude-5.0) return 0; 
	}
	return 1;
}

int is_altitude_still(){
	if(is_still_altitude()){
		altitude_count++;
	}
	else{
		altitude_count = 0;
	}
	if(altitude_count > 1000){
		return 1;
	}
	else{
		return 0;
	}
}

int is_still_gyro(){
	for(unsigned int i=0; i<gyro_array.size(); i++){
		if(gyro_array[i] > 0.5) return 0; 
	}
	return 1;
}

int is_still_acc(){
	float avg_acc = 0.0;
	for(unsigned int i=0; i<acc_array.size(); i++){
		avg_acc += acc_array[i];	
	}
	avg_acc = avg_acc/acc_array.size();
	for(unsigned int i=0; i<acc_array.size(); i++){
		if(acc_array[i] > avg_acc+0.50 || acc_array[i] < avg_acc-0.50) return 0; 
	}
	return 1;
}


int is_Still(){
	if(is_still_altitude() && is_still_gyro() && is_still_acc()){
		return 1;
	}
	else{
	 	return 0;
	}
}

int launch_detect(){
	int count = 0;
	for(unsigned int i=0; i<acc_array.size(); i++){
		if(acc_array[i] > LAUNCH_ACCELERATION) count++;	
	}
	if(count>acc_array.size()*0.5){
		return 1;
	}
	return 0;
}

int launch_stop(){
	int count = 0;
	for(unsigned int i=0; i<acc_array.size(); i++){
		if(acc_array[i] < LAUNCH_ACCELERATION) count++;	
	}
	if(count>acc_array.size()*0.9){
		return 1;
	}
	return 0;
}

int is_descending(){
	float descend_avg_i = 0;
	for(unsigned int i=0; i<50; i++){
		descend_avg_i += altitude_array[i];
	}
	descend_avg_i = descend_avg_i/50;
	
	float descend_avg_f = 0;
	for(unsigned int i=altitude_array.size()-50; i<altitude_array.size(); i++){
		descend_avg_f += altitude_array[i];
	}
	descend_avg_f = descend_avg_f/50;

	if(descend_avg_f+2.0 < descend_avg_i){
		ascend_count = 0;		
		descend_count++;
		if(descend_count > 150){
			return 1;		
		}
	}
	return 0;
}

int is_ascending(){
	float ascend_avg_i = 0;
	for(unsigned int i=0; i<50; i++){
		ascend_avg_i += altitude_array[i];
	}
	ascend_avg_i = ascend_avg_i/50;
	
	float ascend_avg_f = 0;
	for(unsigned int i=altitude_array.size()-50; i<altitude_array.size(); i++){
		ascend_avg_f += altitude_array[i];
	}
	ascend_avg_f = ascend_avg_f/50;

	if(ascend_avg_f-2.0 > ascend_avg_i){
		descend_count = 0;		
		ascend_count++;
		if(ascend_count > 150){
			return 1;		
		}
	}
	return 0;
}

int main(int argc, char *argv[]){
    time_t now = time(0);	
	char* dt = ctime(&now);

	auto begin = chrono::high_resolution_clock::now();

	auto end = chrono::high_resolution_clock::now();


	double elapsed_time = std::chrono::duration_cast<chrono::seconds>(end-begin).count();

	system("echo 1 > /sys/class/gpio/gpio49/value");

	while(elapsed_time < SECS_TO_START){
		end = std::chrono::high_resolution_clock::now();
		elapsed_time = std::chrono::duration_cast<chrono::seconds>(end-begin).count();
	}

	system("echo 0 > /sys/class/gpio/gpio49/value");

	ofstream myfile;
    myfile.open (dt);

	ifstream gpio7;
	string gpioLine;

	//ifstream dataFile;
	//string line;
	//dataFile.open("Mon Apr 14 19:35:22 2014.csv");

    Gyro_Init();
    Accel_Init();
	Baro_Init();
	
	int state = 0;
	int wait1 = 0;
	int still = 0;

	for(int i=0; i<350; i++){
		Read_Barometer();
		Read_Gyro();
		Read_Accel();


		altitude_array.insert(altitude_array.begin(),altitude);
		gyro_array.insert(gyro_array.begin(),gyro_magnitude);
		acc_array.insert(acc_array.begin(),acc_magnitude);
	}
		
		//vector<float> dataVector;
		//split(line, ',', dataVector);
		//float altitude = dataVector[1];		
	


	while(1){

		gpio7.open("/sys/class/gpio/gpio7/value");
		getline(gpio7, gpioLine);
		gpio7.close();
		if(gpioLine == "1") break;		

		Read_Barometer();
		Read_Gyro();
		Read_Accel();

		end = std::chrono::high_resolution_clock::now();

		elapsed_time = chrono::duration_cast<chrono::seconds>(end-begin).count();

		//cout << elapsed_time << endl;

		if(elapsed_time > SECS_TO_DEPLOY && state == 0){
			state = 5;		
		}

		if(elapsed_time > SECS_AFTER_LAUNCH && state != 0){
			break;		
		}

		//myfile << altitude << "," << gyro_magnitude << "," << acc_magnitude  << "," << state << "," << elapsed_time << endl;	
		//cout << "altitude: " << altitude << ", gyro: " << gyro_magnitude << ", acc: " << acc_magnitude  << "," << elapsed_time << "," << state << endl;


		altitude_array.erase(altitude_array.end());
		altitude_array.insert(altitude_array.begin(),altitude);

		gyro_array.erase(gyro_array.end());
		gyro_array.insert(gyro_array.begin(),gyro_magnitude);
		
		acc_array.erase(acc_array.end());
		acc_array.insert(acc_array.begin(),acc_magnitude);

		switch(state){
			case 0:		//start; wait for launch || elapsed time > SECS_TO_DEPLOY
				if(launch_detect()){
					state = 1; 
					system("echo 1 > /sys/class/gpio/gpio48/value");
					begin = std::chrono::high_resolution_clock::now();
				}
				break;
			case 1:		//launch; wait for launch end || wait for descending
				if(launch_stop()){
					state = 2;
					system("echo 0 > /sys/class/gpio/gpio48/value");
					system("echo 1 > /sys/class/gpio/gpio60/value");				
				}
				else if(is_ascending()){
					state = 2;				
					system("echo 0 > /sys/class/gpio/gpio48/value");
					system("echo 1 > /sys/class/gpio/gpio60/value");
				}
				else if(is_descending()){
					state = 4;				
					system("echo 0 > /sys/class/gpio/gpio48/value");
					system("echo 1 > /sys/class/gpio/gpio49/value");
				}
				break;
			case 2:		//ascending; wait for ejection || wait for descending
				is_descending();				
				if(is_ascending()){
					state = 2;				
				}
				else if(acc_magnitude > DEPLOY_ACCELERATION){
					state = 3;				
					system("echo 0 > /sys/class/gpio/gpio60/value");
					system("echo 1 > /sys/class/gpio/gpio48/value");	
				}
				break;
			case 3:		//ejection; wait for descending
				if(is_ascending()){
					state = 2;				
					system("echo 0 > /sys/class/gpio/gpio48/value");
					system("echo 1 > /sys/class/gpio/gpio60/value");
				}				
				if(is_descending()){
					state = 4;
					system("echo 0 > /sys/class/gpio/gpio48/value");
					system("echo 1 > /sys/class/gpio/gpio49/value");				
				}
				break;
			case 4:		//descending; wait for impact
				is_ascending();				
				if(is_descending()){
					state = 4;				
				}
				else if(impact()){
					state = 5;
					descend_count = 0;	
					system("echo 0 > /sys/class/gpio/gpio49/value");
					system("echo 1 > /sys/class/gpio/gpio60/value");			
				}
				break;
			case 5:		//impact; wait for still || wait for descending || wait for long altitude_still
				if(wait1 < 300){
					wait1++;
				}			
				else{	
					is_ascending();
					if(is_descending()){
						state = 4;	
						system("echo 0 > /sys/class/gpio/gpio60/value");
 						system("echo 1 > /sys/class/gpio/gpio49/value");				
					}
					if(is_Still()){
						state = 6;				
					}
					if(is_altitude_still()){
						state = 6;				
					}
				}
				break;
			case 6:		//still
				still = 1;						
				break;
			default:
				state = 1;
		}
		cout << state << endl;
		if(still) break;
	}

	system("echo 1 > /sys/class/gpio/gpio60/value");
	system("echo 1 > /sys/class/gpio/gpio48/value");
	system("echo 1 > /sys/class/gpio/gpio49/value");
	
	begin = std::chrono::high_resolution_clock::now();
	end = std::chrono::high_resolution_clock::now();

	elapsed_time = std::chrono::duration_cast<chrono::seconds>(end-begin).count();

	while(elapsed_time < 10){
		end = std::chrono::high_resolution_clock::now();;

		elapsed_time = std::chrono::duration_cast<chrono::seconds>(end-begin).count();
	}

	system("echo 0 > /sys/class/gpio/gpio60/value");
	system("echo 0 > /sys/class/gpio/gpio48/value");
	system("echo 0 > /sys/class/gpio/gpio49/value");	

	myfile.close();
}	
