#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <SerialPort.h>
#include <exception>
#include <GeographicLib/Geodesic.hpp>
#include <GeographicLib/Constants.hpp>

#include <termios.h>    /*Enables us to set baud rate for RX/TX separately*/
#include <fcntl.h>      /*Enables use of flags to modify open(), read(), write() functions*/

#include "Xbee.h"
#include "serialib.h"


using namespace std;
using namespace GeographicLib;

class GPS
{
	public:
	
		double deslon;
		double deslat;
		
		long double lon;
		double lat;
		
		double headingP1;
		double headingP1C;
		double headingP2;
		double distancia;
		
		SerialPort *serial_GPS;
		serialib LS;
		
		termios port;
		char* tty;
		int fd, bytes_read;
		
		Geodesic *geod;
	
	    GPS();
	    ~GPS();
	    
	    Xbee bee;
	    
		void getData();
		void DMStoDD();
		void getHeadingDistance();
		void getHeading();
		void setconf();
		void changeHeading();
		
		float latitudeDeg;
		float latitudeMin;
		float longitudeDeg;
		float longitudeMin;
		string allData;
		
		int fixIndicator;
		float HDOP;
		
		bool badData;
		
	
};


