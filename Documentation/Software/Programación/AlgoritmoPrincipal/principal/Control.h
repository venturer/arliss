#include <iostream>
#include <string>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <chrono>

#include "INS.h"
#include "RoboClaw.h"
#include "PID.h"
#include "GPS.h"

#define RATE 0.01
#define ToDeg(x) ((x)*57.2957795131)  // *180/pi
#define address 128

using namespace std;

class Control
{
    
    public:
    
    	int veloBase; //velocidad inicial de los motores
		float porcen; //porcentaje de aumento en la velocidad del motor a girar
		int tolerancia; //cantidad de grados de error tolerables en el valor del hearing
		int hor; // 1 horario, -1antihorario
	
		double heading;		//Heading actual
		double hdestinyU;	//Complemento de heading destino
		double hdestiny;	//Hediang destino
		
		PID *controller;
		RoboClaw *roboclaw;
		INS todo;
		GPS gps;
		
		Xbee xb;
		
		void setManualHeadingD();
		void setGPSHeadingD();
		void convertHeading();
		void setPID();
		void initMotors();
		void getHeading();
		int followHeading();
		void stopnav();
		
		Control();
        ~Control();
        
    private:
    
    
};
