#include <stdarg.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <iostream>
#include <cstdlib>
#include <SerialStream.h>
#include <sstream>
#include <stdarg.h>
#include <SerialPort.h>

using namespace std;
using namespace LibSerial;

class Serial
{
    
    public:
    
        SerialStream port;
        char temp;
    
	    // public methods
	    Serial();
	
	    ~Serial();
	    
	    bool writeFail;
		SerialPort *serial_motor;
	    
	    char readS();
	    bool writeS(int value);
	    
	    
};
