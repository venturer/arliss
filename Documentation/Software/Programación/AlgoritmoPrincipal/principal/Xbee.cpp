#include "Xbee.h"


//
// Constructor
//
Xbee::Xbee(){

	serial_Xbee = new SerialPort("/dev/ttyO1");
	
	try {
	  serial_Xbee->Open(SerialPort::BAUD_9600,
		               SerialPort::CHAR_SIZE_8,
		               SerialPort::PARITY_NONE,
		               SerialPort::STOP_BITS_1,
		               SerialPort::FLOW_CONTROL_NONE);
	}

	catch (SerialPort::OpenFailed E) {
	  cout << "Error opening the serial port" << endl;
	  exit (EXIT_FAILURE);
	}
	

	
}

//
// Destructor
//
Xbee::~Xbee(){
}

void Xbee::sendString(string data)
{

  	int count = 0;

	do {
		count++;
		writeFail = false;

		try {
		  serial_Xbee->Write(data);
		  
		}catch (std::runtime_error) {
		  cout << "Error writing the serial port" << endl;
		  writeFail = true;
		}

	}while(writeFail && count < 5);

}
