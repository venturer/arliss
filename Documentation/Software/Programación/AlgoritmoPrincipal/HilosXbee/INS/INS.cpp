#include "INS.h"


//use calibrate_acc to find these values
//take the max and min from each axis
//then use these formulas
//ACC_SC_etc_NEG = 9.8 / axis min
//ACC_SC_etc_POS = 9.8 / axis max
//gravity is sensed differently in each direction
//that is why datasheets list a range of LSB/g
//the values from the datasheets can be used but it will hurt the performance of the filter
//these calibration values will change over time due to factors like tempature
#define ACC_SC_X_NEG 0.001f
#define ACC_SC_X_POS 0.001f
#define ACC_SC_Y_NEG 0.001f
#define ACC_SC_Y_POS 0.001f
#define ACC_SC_Z_NEG 0.001f
#define ACC_SC_Z_POS 0.001f

//use calibrate_mag to find these values
#define compassXMax 301.0f
#define compassXMin -316.0f
#define compassYMax 135.0f
#define compassYMin -451.0f
#define compassZMax 387.0f
#define compassZMin -182.0f
#define inverseXRange (float)(2.0 / (compassXMax - compassXMin))
#define inverseYRange (float)(2.0 / (compassYMax - compassYMin))
#define inverseZRange (float)(2.0 / (compassZMax - compassZMin))

#define M_GNxy 1100 // LSB/gauss
#define M_GNz 980  // LSB/gauss

using namespace std;

auto timer = std::chrono::high_resolution_clock::now();
auto now = std::chrono::high_resolution_clock::now();


INS::INS(){

    Gyro_Init();
    Accel_Init();
    /*
    imu = new openIMU(&gyro_x,&gyro_y,&gyro_z,&accToFilterX,&accToFilterY,&accToFilterZ,&scaledAccX,&scaledAccY,&scaledAccZ,&floatMagX,&floatMagY,&floatMagZ,&rawAltitude,&dt);
*/

    imu = new openIMU(&gyro_x,&gyro_y,&gyro_z,&accToFilterX,&accToFilterY,&accToFilterZ,&floatMagX,&floatMagY,&floatMagZ, &dt);

	
}

INS::~INS(){
}

void INS::Accel_Init()
{
  compass.init();
  compass.enableDefault();
  
  for(int j = 0; j < 300; j++){
    Read_Accel();//to get the smoothing filters caugt up
    usleep(5);
  }

}

void INS::Gyro_Init()
{
  gyro.init();
  gyro.enableDefault();
  
  gyro.writeReg(L3G_CTRL_REG4, 0x20); // 2000 dps full scale
  gyro.writeReg(L3G_CTRL_REG1, 0x0F); // normal power mode, all axes enabled, 100 Hz
  /*
  //this section takes an average of 500 samples to calculate the offset
  //if this step is skipped the IMU will still work, but this simple step gives better results
  offsetX = 0;
  offsetY = 0;
  offsetZ = 0;
  for (int j = 0; j < 500; j ++){//give the internal LPF time to warm up
    Read_Gyro();
    usleep(3);
  }
  for (int j = 0; j < 500; j ++){//give the internal LPF time to warm up
    Read_Gyro();
    gyroSumX += gyro_x;
    gyroSumY += gyro_y;
    gyroSumZ += gyro_z;
    usleep(3);
  }
  offsetX = gyroSumX / 500.0;
  offsetY = gyroSumY / 500.0;
  offsetZ = gyroSumZ / 500.0;*/
}

void INS::Read_Gyro()
{
  gyro.read();
  
  /*
  gyro_x = (gyro.g.x - offsetX) * ToRad(0.07);
  gyro_y = (gyro.g.y - offsetY) * ToRad(0.07);
  //gyro_y *= -1.0;
  gyro_z = (gyro.g.z - offsetZ) * ToRad(0.07);
  //gyro_z *= -1.0;
  
  */
  
  gyro_x = gyro.g.x * ToRad(-0.07);
  gyro_y = gyro.g.y * ToRad(-0.07);
  //gyro_y *= -1.0;
  gyro_z = gyro.g.z * ToRad(-0.07);
  
  
}


// Reads x,y and z accelerometer registers
void INS::Read_Accel()
{
  compass.readAcc();
 
  //the filter expects gravity to be in NED coordinates
  //switching the sign will fix this
  //the raw values are not negated because the accelerometer values will be used with a barometer for altimeter data in a future revisi

  /*
  negated = acc.v.x * -1;
   Smoothing(&negated,&smoothAccX);//this is a very simple low pass digital filter
   negated = acc.v.y * -1;
   Smoothing(&negated,&smoothAccY);//it helps significiantlly with vibrations. 
   negated = acc.v.z * -1;
   Smoothing(&negated,&smoothAccZ);//if the applicaion is not prone to vibrations this can skipped and the raw value simply recast as a float
   */
   
   
  //the order and signs have been switched due to the accelerometer being mounted off by 90 degrees
  //one must be careful to make sure that all of the sensors are in the North, East, Down convention
  accel_x = compass.a.x >> 4;
  Smoothing(&accel_x,&smoothAccX);//this is a very simple low pass digital filter
  accel_y = compass.a.y >> 4; //* -1.0;
  Smoothing(&accel_y,&smoothAccY);//it helps significiantlly with vibrations. 
  accel_z = compass.a.z >> 4; //* -1.0;
  Smoothing(&accel_z,&smoothAccZ);//if the applicaion is not prone to vibrations this can skipped and the raw value simply recast as a float
  
  accToFilterX = -0.001 * smoothAccX;//if the value from the smoothing filter is sent it will not work when the algorithm normalizes the vector
  accToFilterY = -0.001 * smoothAccY;
  accToFilterZ = -0.001 * smoothAccZ;
/*
  if (smoothAccX > 0){
    scaledAccX = smoothAccX * ACC_SC_X_POS;
  }
  else{
    scaledAccX = smoothAccX * ACC_SC_X_NEG;
  }
  if (smoothAccY > 0){
    scaledAccY = smoothAccY * ACC_SC_Y_POS;
  }
  else{
    scaledAccY = smoothAccY * ACC_SC_Y_NEG;
  }
  if (smoothAccZ > 0){
    scaledAccZ = smoothAccZ * ACC_SC_Z_POS;
  }
  else{
    scaledAccZ = smoothAccZ * ACC_SC_Z_NEG;
  }  
  */
  
  
  /*
  	accel_x = compass.a.x >> 4;
  	accel_y = compass.a.y >> 4;
  	accel_z = compass.a.z >> 4;
   
    accToFilterX = accel_x * -0.001;

    accToFilterY = accel_y * -0.001;

    accToFilterZ = accel_z * -0.001;*/

  
}


void INS::Read_Compass()
{
  compass.readMag();
 /*
  floatMagX = ((float)compass.m.x - compassXMin) * inverseXRange - 1.0;
  floatMagY = (((float)compass.m.y - compassYMin) * inverseYRange - 1.0);
  floatMagZ = (((float)compass.m.z - compassZMin) * inverseZRange - 1.0);
  
  */
  
  floatMagX = (float)compass.m.x/(float)M_GNxy;
  floatMagY = (float)compass.m.y/(float)M_GNxy;
  floatMagZ = (float)compass.m.z/(float)M_GNz;
  
}

void INS::Smoothing(int16_t *raw, float *smooth){
  *smooth = (*raw * (0.15)) + (*smooth * 0.85);
}


void INS::IniQuat(){
	Read_Gyro();
	Read_Accel();
	Read_Compass();
	imu->InitialQuat();
	
}

void INS::setGDt(float G){

    dt = G;

}

void INS::update(){

	now = std::chrono::high_resolution_clock::now();
		if(std::chrono::duration_cast<std::chrono::milliseconds>(now-timer).count()>=0.02){   // Main loop runs at 50Hz


			dt = (float)std::chrono::duration_cast<std::chrono::milliseconds>(now-timer).count()/1000;
			timer = std::chrono::high_resolution_clock::now();   


            Read_Gyro();   // This read gyro data
            Read_Accel();     // Read I2C accelerometer
            Read_Compass(); 

			//(todo.imu)->IMUupdate();
			imu->AHRSupdate();
			
		}

/*
			   imu->GetEuler();
			   cout << "G_Dt: " << G_Dt << endl;
			   cout << "Roll: " << todo.imu->roll << endl;
			   cout << "Pitch: " << todo.imu->pitch << endl;
			   cout << "Yaw: " << todo.imu->yaw << endl;
			   cout << "accel_x: " << todo.accToFilterX << endl;
			   cout << "accel_y: " << todo.accToFilterY << endl;
			   cout << "accel_z: " << todo.accToFilterZ << endl;
			   cout << "gx: " << todo.gyro_x << endl;
			   cout << "gy: " << todo.gyro_y << endl;
			   cout << "gz: " << todo.gyro_z << endl;
*/

}



