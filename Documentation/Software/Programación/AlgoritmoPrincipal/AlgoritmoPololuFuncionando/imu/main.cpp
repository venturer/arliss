/*
 * TestApplication.cpp
 *
 * Copyright Derek Molloy, School of Electronic Engineering, Dublin City University
 * www.eeng.dcu.ie/~molloyd/
 *
 * YouTube Channel: http://www.youtube.com/derekmolloydcu/
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL I
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <string>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include "L3G.h"
#include "LPS331.h"
#include "LSM303.h"
#include "AHRS.h"
#include <ctime>
#include <fstream>
#include <time.h>

#define ToDeg(x) ((x)*57.2957795131)  // *180/pi

using namespace std;

int main(int argc, char *argv[]){



/*
    LPS331 baro;
    L3G gyro;
    LSM303 compass;
    
    baro.init();
    gyro.init();
    compass.init();
    
    float pressure;
    float altitude;
    float temperature;
    
    //compass.m_max = {-165,613,2033};
    //compass.m_min = {-1744,-1213,-4096};

    while(1){

	    sleep(2);
	    
	    baro.enableDefault();

	    pressure = baro.readPressureMillibars();
	    altitude = baro.pressureToAltitudeMeters(pressure);
	    temperature = baro.readTemperatureC();
	
	    pressure = baro.readPressureMillibars();
	    altitude = baro.pressureToAltitudeMeters(pressure);
	    temperature = baro.readTemperatureC();

	    cout << "p: " << pressure <<" mbar\ta: " << altitude <<" m\tt: " << temperature <<" deg C" << endl;



	    
	    gyro.enableDefault();
	    gyro.writeReg(L3G_CTRL_REG4, 0x20);

	    gyro.read();
      
      	cout << "[G.X]: " << gyro.g.x << endl;
      	cout << "[G.Y]: " << gyro.g.y << endl;
	    cout << "[G.Z]: " << gyro.g.z << endl;

	    compass.enableDefault();
	    compass.read();

	    cout << "[A.X]: " << compass.a.x << endl;
      	cout << "[A.Y]: " << compass.a.y << endl;
	    cout << "[A.Z]: " << compass.a.z << endl;

	    cout << "[M.X]: " << compass.m.x << endl;
      	cout << "[M.Y]: " << compass.m.y << endl;
	    cout << "[M.Z]: " << compass.m.z << endl;
	    
	    cout << compass.m_max.x << endl;
	    
	    cout <<  "Heading: " << compass.heading() << endl;
	   
	    
	    

    }*/
    
    /*
    LSM303 compass;
    LSM303::vector<int16_t> running_min = {32767, 32767, 32767}, running_max = {-32768, -32768, -32768};

    compass.init();
    compass.enableDefault();
    
    
    while(1){
    
        sleep(0.1);
    
        compass.read();

        running_min.x = min(running_min.x, compass.m.x);
        running_min.y = min(running_min.y, compass.m.y);
        running_min.z = min(running_min.z, compass.m.z);

        running_max.x = max(running_max.x, compass.m.x);
        running_max.y = max(running_max.y, compass.m.y);
        running_max.z = max(running_max.z, compass.m.z);
        
        cout << "min: " << running_min.x << " " << running_min.y << " " << running_min.z << endl;
        cout << "max: " << running_max.x << " " << running_max.y << " " << running_max.z << endl;
    
    }*/
/*    
    AHRS todo;
    
    clock_t timer;
    clock_t timer2;
    double duration;
    float G_Dt=0.02; 

    timer = clock();
    timer2 = clock();
    
    usleep(20);
    int counter=0;
    
    int cont = 0;
    
    string filename = "./data_CalInertialAndMag.csv";  

	ofstream file;
	file.open (filename.c_str(), ios::out | ios::trunc);

	file << "Packet number,Gyroscope X (deg/s),Gyroscope Y (deg/s),Gyroscope Z (deg/s),Accelerometer X (g),Accelerometer Y (g),Accelerometer Z (g),Magnetometer X (G),Magnetometer Y (G),Magnetometer Z (G)" << endl;
    
    time_t start;
    time_t now;
    
    time(&start);
    time(&now);
    
    while(difftime(now,start)<=10){
    
		counter++;

		todo.Read_Gyro();   // This read gyro data
		todo.Read_Accel();     // Read I2C accelerometer
		todo.Read_Compass();
		todo.toReal();

		file << counter << "," << todo.gyroRx << "," << todo.gyroRy << "," << todo.gyroRz << "," << todo.accelRx << "," << todo.accelRy << "," << todo.accelRz << "," << todo.magRx << "," << todo.magRy << "," << todo.magRz << endl;

		time(&now);
    }

    file.close();
    
}*/

	AHRS todo;
    
    clock_t timer;
    clock_t timer_old;
    double duration;
    float G_Dt=0.02; 

    timer = clock();
    
    usleep(20);
    int counter=0;
    
    int cont = 0;

    while(1){
    
        //cout << "timer: " << timer/(double)CLOCKS_PER_SEC << endl;
        //cout << "Clock: " << clock()/(double)CLOCKS_PER_SEC << endl;
       // cout <<  "Dif: " << ((clock()-timer)/((double)CLOCKS_PER_SEC*1000)) << endl;
        if(((clock()-timer)*1000/((double)CLOCKS_PER_SEC))>=20){   // Main loop runs at 50Hz
            counter++;
            timer_old = timer;
            timer=clock();

            if (timer>=timer_old){
              G_Dt = (((float)(timer-timer_old)*1000/((float)CLOCKS_PER_SEC)))/1000.0;    // Real time of loop run. We use this on the DCM algorithm (gyro integration time)
              todo.setGDt(G_Dt);
              
            }else{
              G_Dt = 0;
              todo.setGDt(G_Dt);
            }
            // *** DCM algorithm
            // Data adquisition
            todo.Read_Gyro();   // This read gyro data
            todo.Read_Accel();     // Read I2C accelerometer

            if (counter > 5)  // Read compass data at 10Hz... (5 loop runs)
              {
              counter=0;
              todo.Read_Compass();    // Read I2C magnetometer
              todo.Compass_Heading(); // Calculate magnetic heading  
              }

            // Calculations...
            todo.Matrix_update(); 
            todo.Normalize();
            todo.Drift_correction();
            todo.Euler_angles();
            // ***

            if(cont > 10){
            
               cout << "Tild: " << todo.MAG_Heading_Deg << endl;
               cout << "No Tild: " << todo.compass.heading() << endl;
               
               cout << "Roll: " << ToDeg(todo.roll) << endl;
               cout << "Pitch: " << ToDeg(todo.pitch) << endl;
               cout << "Yaw: " << ToDeg(todo.yaw) << endl;
             
               cout << "gyro x: " << todo.gyro.g.x << endl;
               cout << "gyro y: " << todo.gyro.g.y << endl;
               cout << "gyro z: " << todo.gyro.g.x << endl;
               
               cout << "acce x: " << todo.compass.a.x << endl;
               cout << "acce y: " << todo.compass.a.y << endl;
               cout << "acce z: " << todo.compass.a.z << endl;
               
               cout << "mag x: " << todo.compass.m.x << endl;
               cout << "mag y: " << todo.compass.m.y << endl;
               cout << "mag z: " << todo.compass.m.z << endl;
               
               cont = 0;
            }
            cont++;
      }
    
    }
    
}  

/*
    int cont = 0;
    while(1){


        // *** DCM algorithm
        // Data adquisition
        todo.Read_Gyro();   // This read gyro data
        todo.Read_Accel();     // Read I2C accelerometer

        todo.Read_Compass();    // Read I2C magnetometer
        todo.Compass_Heading(); // Calculate magnetic heading  


        // Calculations...
        todo.Matrix_update(); 
        todo.Normalize();
        todo.Drift_correction();
        todo.Euler_angles();
        // ***

        if(cont > 50){
           cout << "Tild: " << todo.MAG_Heading_Deg << endl;
           cout << "No Tild: " << todo.compass.heading() << endl;
           
           cout << "Roll: " << ToDeg(todo.roll) << endl;
           cout << "Pitch: " << ToDeg(todo.pitch) << endl;
           cout << "Yaw: " << ToDeg(todo.yaw) << endl;
           cont = 0;
        }
        cont++;
    } 
    */
  
    


