#include <iostream>
#include "L3G.h"
#include "LPS331.h"
#include "LSM303.h"

using namespace std;

class AHRS
{
    
    public:
    

        L3G gyro;
        LSM303 compass;
        LPS331 baro;
        
        float gyro_x, gyro_y, gyro_z;
        float accel_x, accel_y, accel_z;
        int16_t magnetom_x, magnetom_y, magnetom_z;
        float c_magnetom_x;
        float c_magnetom_y;
        float c_magnetom_z;
        float MAG_Heading;
        float MAG_Heading_Deg;
        
        float accelRx, accelRy, accelRz;
        float gyroRx, gyroRy, gyroRz;
        float magRx, magRy, magRz;
        

        // Euler angles
        float roll;
        float pitch;
        float yaw;

        
        AHRS();
        ~AHRS();
        
        void Gyro_Init();
        void Read_Gyro();
        void Accel_Init();
        void Read_Accel();
        void Read_Compass();
        
        
        void Normalize();
        void Drift_correction();
        void Matrix_update();
        void Matrix_Multiply(float a[3][3], float b[3][3],float mat[3][3]);
        
        void Euler_angles();
        void Compass_Heading();
        
        float Vector_Dot_Product(float vector1[3],float vector2[3]);
        void Vector_Cross_Product(float vectorOut[3], float v1[3],float v2[3]);
        void Vector_Scale(float vectorOut[3],float vectorIn[3], float scale2);
        void Vector_Add(float vectorOut[3],float vectorIn1[3], float vectorIn2[3]);
        float constrain(float x, int a, int b);
        void setGDt(float G);
        void toReal();
        

};
