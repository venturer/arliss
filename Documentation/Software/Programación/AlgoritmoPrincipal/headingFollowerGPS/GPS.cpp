#include "GPS.h"


//
// Constructor
//
GPS::GPS(){

	geod = new Geodesic(Constants::WGS84_a(), 1/298.257223563);
	serial_GPS = new SerialPort("/dev/ttyO4");
	
	try {
	  serial_GPS->Open(SerialPort::BAUD_57600,
		               SerialPort::CHAR_SIZE_8,
		               SerialPort::PARITY_NONE,
		               SerialPort::STOP_BITS_1,
		               SerialPort::FLOW_CONTROL_NONE);
	}

	catch (SerialPort::OpenFailed E) {
	  cout << "Error opening the serial port" << endl;
	  exit (EXIT_FAILURE);
	}
	
	deslon = -84.0;
	deslat = 9.0;
	
}

//
// Destructor
//
GPS::~GPS(){
}

void GPS::getHeading(){

	getData();
	DMStoDD();
	getHeadingDistance();

}

void GPS::DMStoDD(){

	lon = -1*(longitudeDeg + (longitudeMin/60));
	lat = latitudeDeg + (latitudeMin/60);

}

void GPS::getHeadingDistance(){

	geod->Inverse(lat, lon, deslat, deslon, distancia, headingP1, headingP2);

}


void GPS::getData(){
	
	//-- ASCII string to send through the serial port
	bool notValid;
	SerialPort::DataBuffer bufferGPS;
	int count = 0;

	do{
		notValid = false;
		count++;

		//-- Wait for the received data
		try{
	  		serial_GPS->Read(bufferGPS,72,100);
		}
		catch (SerialPort::ReadTimeout E) {
			cout << "TIMEOUT!" << endl;
			notValid = true;
			break;
		}

		if(bufferGPS[0] != '$'){
			notValid = true;    
		}else if( bufferGPS[22] != '.' ){
			notValid = true;    
		}else if( bufferGPS[35] != '.'){
			notValid = true;    
	}
	}while(notValid && (count <= 5));
	

	if(notValid){
	  badData = true;

	}else {
	
		string str(bufferGPS.begin(),bufferGPS.end());
		cout << str << endl;
		
		latitudeDeg = atof((str.substr(18,2)).c_str());
		latitudeMin = atof((str.substr(20,7)).c_str());
		
		longitudeDeg = atof((str.substr(30,3)).c_str());
		longitudeMin = atof((str.substr(33,7)).c_str());
		
		fixIndicator = atoi((str.substr(43,1)).c_str());
		HDOP = atof((str.substr(47,4)).c_str());
		
		badData = false;
		
	} 
}


void GPS::setconf(){

	serial_GPS->Write("$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n"); //solo envie GGA

	serial_GPS->Write("$PMTK220,200*2C\r\n"); // 5 Hz

	serial_GPS->Write("$PMTK313,1*2E\r\n"); // Enable to search a SBAS satellite

	serial_GPS->Write("$PMTK301,2*2E\r\n"); // Enable WAAS as DGPS Source

}

