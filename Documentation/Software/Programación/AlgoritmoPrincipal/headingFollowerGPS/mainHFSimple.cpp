#include <iostream>
#include <string>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include "L3G.h"
#include "LPS331.h"
#include "LSM303.h"
#include "AHRS.h"
#include "RoboClaw.h"
#include <ctime>
#include <fstream>
#include <time.h>
#include <stdlib.h> 
#include <cmath>
#include <chrono>
#include "PID.h"
 
#define RATE 0.1
 

#define ToDeg(x) ((x)*57.2957795131)  // *180/pi

#define address 128

using namespace std;


int main(int argc, char *argv[]){


    int veloBase = 30; //velocidad inicial de los motores
	float porcen = 0.15; //porcentaje de aumento en la velocidad del motor a girar
	int intervalo = 500; // tiempo en ms a esperar para la correccion
	int tolerancia = 5; //cantidad de grados de error tolerables en el valor del hearing
	int hor = -1; // 1 horario, -1antihorario
	
	double heading;
	double hdestiny = -122;
/*	
	//Kc, Ti, Td, interval
	PID controller(1.0, 0.0, 0.0, RATE);
	//Analog input from 0.0 to 3.3V
	controller.setInputLimits(-180.0, 180.0);
	//Pwm output from 0.0 to 1.0
	controller.setOutputLimits(0.0, 1.0);
	//If there's a bias.
	//controller.setBias(0.3);
	controller.setMode(AUTO_MODE);
	//We want the process variable to be 1.7V
	controller.setSetPoint(hdestiny);*/

	RoboClaw *roboclaw = new RoboClaw;
	roboclaw->SetMinVoltageMainBattery(address,2);
	roboclaw->ForwardMixed(address,veloBase);

	
	AHRS todo;    
    usleep(20);

    while(1){
    
    	todo.updateIMU();
    	heading = ToDeg(todo.yaw);
    	
    	//Revisa tolerancia
		if(abs(heading-hdestiny) > tolerancia){
		
		    //caso giro horario
			if( ((hor*(heading-hdestiny) < 0)&&(abs(heading-hdestiny) < 180)) || ( (hor*(heading-hdestiny) > 0)&&(abs(heading-hdestiny) > 180)) ){
			    cout << "hdestiny: " << hdestiny << "heading: " << heading << endl;
				cout << "caso 1: " << abs(heading-hdestiny) << endl;
				roboclaw->ForwardM1(address, veloBase-(porcen/2)*veloBase);
				roboclaw->ForwardM2(address, veloBase+(porcen/2)*veloBase);
				continue;						
			}
			//caso giro antihorario
			if( ((hor*(heading-hdestiny) > 0)&&(abs(heading-hdestiny) < 180)) || ( (hor*(heading-hdestiny) < 0)&&(abs(heading-hdestiny) > 180)) ){
				cout << "hdestiny: " << hdestiny << "heading: " << heading << endl;
				cout << "caso 2: " << abs(heading-hdestiny) << endl;
				roboclaw->ForwardM1(address, veloBase+(porcen/2)*veloBase);
				roboclaw->ForwardM2(address, veloBase-(porcen/2)*veloBase);
				continue;
			}
		
		}else{
		
			roboclaw->ForwardMixed(address,veloBase);
		}
    
    }
    
}  


  
    


