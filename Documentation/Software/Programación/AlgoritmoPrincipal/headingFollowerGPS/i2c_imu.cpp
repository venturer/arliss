#include "i2c_imu.h"
#include <iostream>
using namespace std;

void i2cIMU::init(unsigned char I2CBus, unsigned char device){
	DEVICE_ADDR = device;
	DEVICE_BUS = I2CBus;
	this->i2cOpen();
}

// open the Linux device
void i2cIMU::i2cOpen()
{
	//cout << "beagle-i2c opening /dev/i2c-3... ";
	char namebuf[MAX_BUFFER_SIZE];
	snprintf(namebuf, sizeof(namebuf), "/dev/i2c-%d", DEVICE_BUS);
	g_i2cFile = open(namebuf, O_RDWR);
	if (g_i2cFile < 0) {
		perror("i2cOpen in i2cIMU::i2cOpen");
		exit(1);
	}
//	else cout << "OK"<<endl;
}

// close the Linux device
void i2cIMU::i2cClose()
{
	close(g_i2cFile);
}

// set the I2C slave address for all subsequent I2C device transfers
void i2cIMU::i2cSetAddress()
{
	//cout << "beagle-i2c setting address 0x"<< hex <<(int)address <<"... ";
	if (ioctl(g_i2cFile, I2C_SLAVE, DEVICE_ADDR) < 0) {
		perror("i2cSetAddress error in i2cIMU::i2cSetAddress");
		exit(1);
	}
//	else cout << "OK" <<endl;
}


void i2cIMU::Send_I2C_Byte(unsigned char Reg_ADDR, unsigned char Data){
	i2cSetAddress();
	//cout << "beagle-i2c writing 0x"<< hex << (int)Data <<" to 0x"<<hex <<(int)DEVICE_ADDR << ", reg 0x" <<hex<<(int)Reg_ADDR <<"... ";
	I2C_WR_Buf[0] = Reg_ADDR;
	I2C_WR_Buf[1] = Data;

	if(write(g_i2cFile, I2C_WR_Buf, 2) != 2) {
		perror("Write Error in i2cIMU::Send_I2C_Byte");
	}
//	else cout << "OK";

}


uint8_t i2cIMU::Read_I2C_Byte(unsigned char Reg_ADDR){
	I2C_WR_Buf[0] = Reg_ADDR;

	i2cSetAddress();
	if(write(g_i2cFile, I2C_WR_Buf, 1) != 1) {
		perror("Write Error in i2cIMU::Read_I2C_Byte");
		printf("%x\n", Reg_ADDR);
	}
	i2cSetAddress();	
	if(read(g_i2cFile, I2C_RD_Buf, 1) !=1){
		perror("Read Error i2cIMU::Read_I2C_Byte");
		printf("%x\n", Reg_ADDR);
	}

	return I2C_RD_Buf[0];
}

uint8_t i2cIMU::Read_Multi_Byte(unsigned char Reg_ADDR, size_t n){
	I2C_WR_Buf[0] = Reg_ADDR;

	i2cSetAddress();
	ssize_t s = write(g_i2cFile, I2C_WR_Buf, 1);
	if( s != 1) {
		cout << "Wanted to write " << 1 << " byte, but instead wrote " << s << ". " <<endl;
		perror("Write Error in myI2C::Read_Multi_Byte");
	}
	i2cSetAddress();	
	ssize_t t = read(g_i2cFile, I2C_RD_Buf, n);
	if( t != n)
	{
		cout << "Wanted to read " << n << " bytes, but instead got " << t << ". " <<endl;
		perror("Read Error in myI2C::Read_Multi_Byte");
	}

	return I2C_RD_Buf[0];
}

uint8_t *i2cIMU::Read_Full_Byte(){

	uint8_t *fullBuffer = new uint8_t[MAX_BUFFER_FULL];

	for(int i=0; i<MAX_BUFFER_FULL; i++){	
		
		I2C_WR_Buf[0] = i;

		i2cSetAddress();
		if(write(g_i2cFile, I2C_WR_Buf, 1) != 1) {
			perror("Write Error in i2cIMU::Read_I2C_Byte");
		}
		i2cSetAddress();	
		if(read(g_i2cFile, I2C_RD_Buf, 1) !=1){
			perror("Read Error i2cIMU::Read_I2C_Byte");
		}

		fullBuffer[i] = I2C_RD_Buf[0];
	}

	return fullBuffer;
}
