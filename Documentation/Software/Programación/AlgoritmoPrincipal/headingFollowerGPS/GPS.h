#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <SerialPort.h>
#include <exception>
#include <GeographicLib/Geodesic.hpp>
#include <GeographicLib/Constants.hpp>


using namespace std;
using namespace GeographicLib;

class GPS
{
	public:
	
		double deslon;
		double deslat;
		
		double lon;
		double lat;
		
		double headingP1;
		double headingP2;
		double distancia;
		
		SerialPort *serial_GPS;
		
		Geodesic *geod;
	
	    GPS();
	    ~GPS();
	    
		void getData();
		void DMStoDD();
		void getHeadingDistance();
		void getHeading();
		void setconf();
		
		float latitudeDeg;
		float latitudeMin;
		float longitudeDeg;
		float longitudeMin;
		
		int fixIndicator;
		float HDOP;
		
		bool badData;
		
	
};


