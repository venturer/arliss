#ifndef I2C_IMU_H_
#define I2C_IMU_H_

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <stdint.h>

#define MAX_BUFFER_SIZE					64
#define MAX_BUFFER_FULL					0x80

class i2cIMU {
	int g_i2cFile;
public:

	void init(unsigned char I2CBus, unsigned char device);

	// Public Variables
	uint8_t I2C_WR_Buf[MAX_BUFFER_SIZE];			// Contains data you want to send
	uint8_t I2C_RD_Buf[MAX_BUFFER_SIZE];			// Contains data which was read
	unsigned char DEVICE_ADDR;
	unsigned char DEVICE_BUS;

	// Initialize Functions
	void i2cOpen();										// Opens i2cbus 3, done at the beginning
	void i2cClose();									// Closes i2cbus 3, done at the ending
	void i2cSetAddress();					// Changes device address

	// Sends a single byte <Data> to <DEVICE_ADDR> on the register <Reg_ADDR>
	void Send_I2C_Byte(unsigned char Reg_ADDR, unsigned char Data);	

	// Reads and returns a single byte from <DEVICE_ADDR> on the register <Reg_ADDR>
	uint8_t Read_I2C_Byte(unsigned char Reg_ADDR);

	// Reads multipes byte from <DEVICE_ADDR> starting from the register address <Reg_ADDR>.
	// Read the output from i2cptr->I2C_RD_Buf
	uint8_t Read_Multi_Byte(unsigned char Reg_ADDR, size_t n);

	uint8_t *Read_Full_Byte();
};
#endif /* I2C_IMU_H_ */
