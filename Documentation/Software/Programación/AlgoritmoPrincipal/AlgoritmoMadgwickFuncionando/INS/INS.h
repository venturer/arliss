#include <iostream>
#include <ctime>
#include <math.h>
#include <cmath>
#include <unistd.h>

#include "L3G.h"
#include "LPS331.h"
#include "LSM303.h"
#include "openIMU.h"


using namespace std;

class INS
{
    
    public:
    
        L3G gyro;
        LSM303 compass;
        LPS331 baro;
        
               
		//for the calibration of the gyro
		int32_t gyroSumX,gyroSumY,gyroSumZ;
		int16_t offsetX,offsetY,offsetZ;

        float gyro_x, gyro_y, gyro_z;
        int16_t accel_x,accel_y,accel_z;
        int magnetom_x;
        int magnetom_y;
        int magnetom_z;
        float c_magnetom_x;
        float c_magnetom_y;
        float c_magnetom_z;
        float MAG_Heading;
        float MAG_Heading_Deg;
        
        float scaledAccX,scaledAccY,scaledAccZ;
        float smoothAccX,smoothAccY,smoothAccZ;
        float accToFilterX,accToFilterY,accToFilterZ;
        float floatMagX,floatMagY,floatMagZ;//needs to be a float so the vector can be normalized
        float rawAltitude;
        float dt;
        
        openIMU *imu;

        INS();
        ~INS();
        
        void Gyro_Init();
        void Read_Gyro();
        void Accel_Init();
        void Read_Accel();
        void Read_Compass();
        
        void IniQuat();
        void setGDt(float G);
        
        void Smoothing(int16_t *raw, float *smooth);
        

};
