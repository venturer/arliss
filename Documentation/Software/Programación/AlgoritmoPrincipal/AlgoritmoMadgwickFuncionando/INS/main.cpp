#include <iostream>
#include <string>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include "L3G.h"
#include "LPS331.h"
#include "LSM303.h"
#include "INS.h"
#include <ctime>

using namespace std;

int main(int argc, char *argv[]){
 
    INS todo;
    
    //todo.IniQuat();
    
    clock_t timer;
    clock_t print;
    print = clock();
    timer = clock();
    
    float G_Dt=0.02; 
    usleep(20);

	
	
    while(1){
    
        
        if(((clock()-timer)*1000/((float)CLOCKS_PER_SEC)) >= 5){   // Main loop runs at 50Hz

			//cout << "timer: " << timer << endl;
		    //cout << "Clock: " << clock() << endl;
		    //cout <<  "Dif: " << ((float)(clock()-timer)*1000/((float)CLOCKS_PER_SEC)) << endl;			
			
			G_Dt = ((float)(clock()-timer)/((float)CLOCKS_PER_SEC));   
            todo.setGDt(G_Dt);
			
            timer=clock();


            todo.Read_Gyro();   // This read gyro data
            todo.Read_Accel();     // Read I2C accelerometer
            todo.Read_Compass(); 

			//(todo.imu)->IMUupdate();
			(todo.imu)->AHRSupdate();
			
		}

			if(((clock()-print)/((float)CLOCKS_PER_SEC)) > 0.1){
			   
			   print = clock();
			   
			   todo.imu->GetEuler();
			   cout << "G_Dt: " << G_Dt << endl;
			   cout << "Roll: " << todo.imu->roll << endl;
			   cout << "Pitch: " << todo.imu->pitch << endl;
			   cout << "Yaw: " << todo.imu->yaw << endl;
			   cout << "accel_x: " << todo.accToFilterX << endl;
			   cout << "accel_y: " << todo.accToFilterY << endl;
			   cout << "accel_z: " << todo.accToFilterZ << endl;
			   cout << "gx: " << todo.gyro_x << endl;
			   cout << "gy: " << todo.gyro_y << endl;
			   cout << "gz: " << todo.gyro_z << endl;
			

		} 
    }
    
}

