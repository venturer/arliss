#include "GPS.h"


//
// Constructor
//
GPS::GPS(){

	geod = new Geodesic(Constants::WGS84_a(), 1/298.257223563);	
	
	tty = "/dev/ttyO4";

	if ((fd = open(tty, O_RDWR | O_NOCTTY)) < 0){
        cout << "Could not open port." << endl;
    }

	/* Setting other Port Stuff */
    memset(&port,0,sizeof(port));

    if (tcgetattr(fd, &port) != 0){ /* Obtain current terminal device settings in order to modify them at a later time. */
        cout << "Unable to retrieve port attributes." << endl;
    }

    if (cfsetispeed(&port, B57600) < 0){ /* Sets baud rate to 9600 */
        cout << "Input baud rate not successfully set." << endl;
    }
    if (cfsetospeed(&port, B57600) < 0){
        cout << "Input baud rate not successfully set." << endl;
    }

	port.c_cflag = (port.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
    port.c_iflag &= ~IGNBRK;         // disable break processing
    port.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
    port.c_oflag = 0;                // no remapping, no delays
    port.c_cc[VMIN]  = 0;            // read doesn't block
    port.c_cc[VTIME] = 1;            // 0.5 seconds read timeout

    port.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    port.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
    port.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    port.c_cflag &= ~CSTOPB;
    port.c_cflag &= ~CRTSCTS;
	
	if ( tcsetattr ( fd, TCSANOW, &port ) != 0){
		cout << "Error from tcsetattr" << endl;
	}
	
	deslon = -84.0;
	deslat = 9.0;
	
	
	
}

//
// Destructor
//
GPS::~GPS(){
}

void GPS::getHeading(){

	getData();
	DMStoDD();
	getHeadingDistance();

}

void GPS::DMStoDD(){

	//cout << "longiMin: "<< longitudeMin << endl;
	//cout << "longiMin60: "<< longitudeMin/60 << endl;

	lon = -1*(long double)((long double)longitudeDeg + ((long double)longitudeMin/60));
	
	//cout << "lon: "<< lon << endl;
	
	lat = latitudeDeg + (latitudeMin/60);

}

void GPS::getHeadingDistance(){

	geod->Inverse(lat, lon, deslat, deslon, distancia, headingP1, headingP2);

}

void GPS::changeHeading(){

	if(headingP1 < 0){		
		headingP1C = 360 + headingP1;
	}else{
	
		headingP1C = headingP1;
	}

}


void GPS::getData(){
	
	bool notValid;
	SerialPort::DataBuffer bufferGPS;
	int count = 0;
	string str = "";
	string RMC = "";
	
	int Ret;                                                                // Used for return values
    char Buffer[300];
	

	do{
		int n = 0;
		int r = 0;

		notValid = false;
		count++;
		char buf;
		str = "";
		RMC = "";

		do {
			n = read( fd, &buf, 1 );
			str.push_back( buf );
			
			if(str == "\n"){
				str = "";
				n = read( fd, &buf, 1 );
				str.push_back( buf );
			}

		} while( buf != '\n' && n > 0);
		
		if(str[0] != '$'){
			notValid = true;    
		}else if( str[22] != '.' ){
			notValid = true;    
		}else if( str[35] != '.'){
			notValid = true;   
		}
	}while(notValid && (count <= 5));
		
	if(!notValid){		
		int n = 0;
		char buf;
		do {
			n = read( fd, &buf, 1 );
			RMC.push_back( buf );
		
			if(RMC == "\n"){
				RMC = "";
				n = read( fd, &buf, 1 );
				RMC.push_back( buf );
			}

		} while( buf != '\n' && n > 0);

	}
		
	
	
	if(notValid){
	  badData = true;

	}else {
	
		//string str(bufferGPS.begin(),bufferGPS.end());
		//cout << str << endl;
		
		bee.sendString(RMC);
		bee.sendString("\r\n");
		
		allData = str;
		
		latitudeDeg = atof((str.substr(18,2)).c_str());
		latitudeMin = atof((str.substr(20,7)).c_str());
		
		longitudeDeg = atof((str.substr(30,3)).c_str());
		longitudeMin = atof((str.substr(33,7)).c_str());
		
		fixIndicator = atoi((str.substr(43,1)).c_str());
		HDOP = atof((str.substr(47,4)).c_str());
		
		badData = false;
		
	}
	//bee.sendString(RMC);
	 
}


void GPS::setconf(){


	int size;
	
	size = strlen("$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"); //solo envie GGA
    write(fd, "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n", size);
    
    size = strlen("$PMTK220,200*2C\r\n"); // 5 Hz
    write(fd, "$PMTK220,200*2C\r\n", size);
    
    size = strlen("$PMTK313,1*2E\r\n"); // Enable to search a SBAS satellite
    write(fd, "$PMTK313,1*2E\r\n", size);
    
    size = strlen("$PMTK301,2*2E\r\n"); // Enable WAAS as DGPS Source*/
    write(fd, "$PMTK301,2*2E\r\n", size);
    
	


}

