#include "Control.h"

using namespace std;

Control::Control(){

	veloBase = 70; //velocidad inicial de los motores
	porcen = 0.15; //porcentaje de aumento en la velocidad del motor a girar
	tolerancia = 1; //cantidad de grados de error tolerables en el valor del hearing
	hor = -1; // 1 horario, -1antihorario
	
	gps.setconf();
	

}

Control::~Control(){
}

void Control::setManualHeadingD(){

	hdestinyU = 30;

}

void Control::setGPSHeadingD(){

	gps.getHeading();
	if(gps.badData == false && gps.HDOP <= 2){
		//cout << "headingP: " << gps.headingP1 << endl;
		gps.changeHeading();
		//cout << "headingPCahnge: " << gps.headingP1C << endl;
		hdestinyU = gps.headingP1C;		
	}
}

void Control::convertHeading(){

	if(hdestinyU > 180){
		hdestiny = hdestinyU-180;
	}else{
		hdestiny = hdestinyU+180;
	}

}

void Control::setPID(){

	controller = new PID(1.0, 0.0, 0.0, RATE);
	//Analog input from 0.0 to 3.3V
	controller->setInputLimits(0,181);
	//Pwm output from 0.0 to 1.0
	controller->setOutputLimits(0, 1.6);
	//If there's a bias.
	//controller.setBias(0.3);
	controller->setMode(AUTO_MODE);
	//We want the process variable to be 1.7V
	controller->setSetPoint(180);

}

void Control::initMotors(){

	roboclaw = new RoboClaw;
	roboclaw->SetMinVoltageMainBattery(address,2);
	roboclaw->ForwardMixed(address,veloBase);

}

void Control::getHeading(){

	todo.update();
	(todo.imu)->GetEuler();
	heading = (todo.imu)->yaw;
	//cout << "heading: " << heading << endl;
}

int Control::followHeading(){

	//Revisa tolerancia
		if(abs(heading-hdestiny) > tolerancia){
		
			float diff = abs(heading-hdestiny);
			
			if(diff > 180 ){
				diff = abs(360-diff);
			}

			controller->setProcessValue(diff);
			porcen = controller->compute();

		    //caso giro horario
			if( ((hor*(heading-hdestiny) < 0)&&(abs(heading-hdestiny) < 180)) || ( (hor*(heading-hdestiny) > 0)&&(abs(heading-hdestiny) > 180)) ){
			    //cout << "hdestiny: " << hdestiny << "heading: " << heading << endl;
				//cout << "caso 1: " << abs(heading-hdestiny) << endl;

				//cout << "caso 1 -> Motor1 = " << (veloBase-(porcen/2)*veloBase) << " Motor2 = " << veloBase+(porcen/2)*veloBase << endl;

				roboclaw->ForwardM1(address, veloBase-(porcen/2)*veloBase);
				roboclaw->ForwardM2(address, veloBase+(porcen/2)*veloBase);

				return 1;						
			}
			//caso giro antihorario
			if( ((hor*(heading-hdestiny) > 0)&&(abs(heading-hdestiny) < 180)) || ( (hor*(heading-hdestiny) < 0)&&(abs(heading-hdestiny) > 180)) ){
				//cout << "hdestiny: " << hdestiny << "heading: " << heading << endl;
				//cout << "caso 2: " << abs(heading-hdestiny) << endl;

				//cout << "caso 2 -> Motor1 = " << veloBase-(porcen/2)*veloBase << " Motor2 = " << veloBase+(porcen/2)*veloBase << endl;
				

				roboclaw->ForwardM1(address, veloBase+(porcen/2)*veloBase);
				roboclaw->ForwardM2(address, veloBase-(porcen/2)*veloBase);

				return 1;
			}
		
		}else{
		
			roboclaw->ForwardMixed(address,veloBase);
			return 0;
		}
		
		//cout << "porcen: " << porcen << endl;
}

void Control::stopnav(){
	
	roboclaw->ForwardM1(address, 0);
	roboclaw->ForwardM2(address, 0);

}
