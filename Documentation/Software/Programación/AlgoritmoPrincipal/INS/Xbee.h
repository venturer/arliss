#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <SerialPort.h>
#include <exception>
#include <GeographicLib/Geodesic.hpp>
#include <GeographicLib/Constants.hpp>


using namespace std;
using namespace GeographicLib;

class Xbee
{
	public:

		bool writeFail;
		SerialPort *serial_Xbee;
		
		Xbee();
	    ~Xbee();
	    
	    void sendString(string);	
	
};
