#include <iostream>
#include <string>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <ctime>
#include <boost/thread.hpp>
#include <limits>

#include "Control.h"



using namespace std;
using namespace boost;


Control all;

void upHeading(){

	while(1){
	
		all.getHeading();
	}

}

void upGPS(){

	while(1){
		all.setGPSHeadingD();
		all.convertHeading();
		all.followHeading();
	}
}

void lol(){

	while(1){
		//usleep(10);
		//all.followHeading();
	}

}


int main(){

	usleep(20);
	
	all.gps.setconf();
	all.setManualHeadingD();
	all.convertHeading();
	all.setPID();
	all.initMotors();
	
	sleep(1);
	
	//cout << "lol" << endl;
	
	all.gps.deslat = 9.937416666666667;
	all.gps.deslon = -84.04421666666667;
/*
	thread tgps = thread(upGPS);
    thread timu = thread(upHeading);
    thread lol2 = thread(lol);*/
    
    ofstream file;
    file.open ("datos.txt", ios::out | ios::trunc);
    
    cout.precision(16);
    file.precision(16);
	
	
	while(1){
	
		all.getHeading();
		file << "Heading: " << all.heading << endl;
		//cout << "Heading: " << all.heading << endl;
		all.setGPSHeadingD();
		all.convertHeading();
		
		//cout << all.gps.deslat << endl;
		//cout << all.gps.deslon << endl;
		
		file << all.gps.allData << endl;
		file << "HeadingP1: " << all.gps.headingP1 << endl;
		file << "HeadingP1C: " << all.gps.headingP1C << endl;
		file << "HeadingP2: " << all.gps.headingP2 << endl;
		file << "distancia: " << all.gps.distancia<< endl;
		file << "lat: " << all.gps.lat<< endl;
		file << "lon: " << all.gps.lon<< endl;

		if(all.gps.distancia > 2){
		
			all.followHeading();
			
		}else{
		
			all.stopnav();
			return 0;
		
		}
	}
	

}
/*
Control all;

int main(){

	while(1){
		sleep(0.5);
		all.gps.bee.sendString("hola\n");
	
	}

}*/

