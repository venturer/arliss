#include <iostream>
#include <string>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include "L3G.h"
#include "LPS331.h"
#include "LSM303.h"
#include "AHRS.h"
#include "RoboClaw.h"
#include <ctime>
#include <fstream>
#include <time.h>
#include <stdlib.h> 
#include <cmath>
#include <chrono>
#include "PID.h"
 
#define RATE 0.01
 

#define ToDeg(x) ((x)*57.2957795131)  // *180/pi

#define address 128

using namespace std;

/*
int main(int argc, char *argv[]){


    int veloBase = 50; //velocidad inicial de los motores
	float porcen = 0.15; //porcentaje de aumento en la velocidad del motor a girar
	float porcen2 = 0.15;
	int intervalo = 500; // tiempo en ms a esperar para la correccion
	int tolerancia = 1; //cantidad de grados de error tolerables en el valor del hearing
	int hor = -1; // 1 horario, -1antihorario
	
	double heading;
	double hdestiny = 39;
	
	//Kc, Ti, Td, interval
	PID controller(1.0, 0.0, 0.0, RATE);
	//Analog input from 0.0 to 3.3V
	controller.setInputLimits(0,hdestiny+1);
	//Pwm output from 0.0 to 1.0
	controller.setOutputLimits(0, 1.0);
	//If there's a bias.
	//controller.setBias(0.3);
	controller.setMode(AUTO_MODE);
	//We want the process variable to be 1.7V
	controller.setSetPoint(abs(hdestiny));
	
	//Kc, Ti, Td, interval
	PID controller2(1.0, 0.0, 0.0, RATE);
	//Analog input from 0.0 to 3.3V
	controller2.setInputLimits(0,(360-hdestiny)+1);
	//Pwm output from 0.0 to 1.0
	controller2.setOutputLimits(0, 1.0);
	//If there's a bias.
	//controller.setBias(0.3);
	controller2.setMode(AUTO_MODE);
	//We want the process variable to be 1.7V
	controller2.setSetPoint(abs((360-hdestiny)));
	

	RoboClaw *roboclaw = new RoboClaw;
	roboclaw->SetMinVoltageMainBattery(address,2);
	roboclaw->ForwardMixed(address,veloBase);

	
	AHRS todo;    
    usleep(20);

    while(1){
    
    	todo.updateIMU();
    	//heading = ToDeg(todo.yaw);
    	heading = todo.compass.heading();
    	cout << "Tild: " << todo.MAG_Heading_Deg << endl;
        cout << "No Tild: " << todo.compass.heading() << endl;
    	
    	controller.setProcessValue(abs(todo.compass.heading()));
    	//controller.setProcessValue(todo.compass.heading());
    	porcen = controller.compute();
    	
    	controller2.setProcessValue(abs(360-todo.compass.heading()));
    	//controller.setProcessValue(todo.compass.heading());
    	porcen2 = controller2.compute();
    	
    	cout << "porcen: " << porcen << endl;
    	cout << "porcen2: " << porcen2 << endl;
    	cout << "heading: " << heading << endl;
   	
    	//Revisa tolerancia
		if(abs(heading-hdestiny) > tolerancia){
		
		    //caso giro horario
			if( ((hor*(heading-hdestiny) < 0)&&(abs(heading-hdestiny) < 180)) || ( (hor*(heading-hdestiny) > 0)&&(abs(heading-hdestiny) > 180)) ){
			    //cout << "hdestiny: " << hdestiny << "heading: " << heading << endl;
				//cout << "caso 1: " << abs(heading-hdestiny) << endl;
				cout << "caso 1 -> Motor1 = " << (veloBase-(porcen2/2)*veloBase) << " Motor2 = " << veloBase+(porcen2/2)*veloBase << endl;
				roboclaw->ForwardM1(address, veloBase+(porcen2/2)*veloBase);
				roboclaw->ForwardM2(address, veloBase-(porcen2/2)*veloBase);
				continue;						
			}
			//caso giro antihorario
			if( ((hor*(heading-hdestiny) > 0)&&(abs(heading-hdestiny) < 180)) || ( (hor*(heading-hdestiny) < 0)&&(abs(heading-hdestiny) > 180)) ){
				//cout << "hdestiny: " << hdestiny << "heading: " << heading << endl;
				//cout << "caso 2: " << abs(heading-hdestiny) << endl;
				cout << "caso 2 -> Motor1 = " << veloBase-(porcen/2)*veloBase << " Motor2 = " << veloBase+(porcen/2)*veloBase << endl;
				roboclaw->ForwardM1(address, veloBase-(porcen/2)*veloBase);
				roboclaw->ForwardM2(address, veloBase+(porcen/2)*veloBase);
				continue;
			}
		
		}else{
		
			roboclaw->ForwardMixed(address,veloBase);
		}
    
    }
    
}  */

int main(int argc, char *argv[]){


    int veloBase = 50; //velocidad inicial de los motores
	float porcen = 0.15; //porcentaje de aumento en la velocidad del motor a girar
	float porcen2 = 0.15;
	int intervalo = 500; // tiempo en ms a esperar para la correccion
	int tolerancia = 1; //cantidad de grados de error tolerables en el valor del hearing
	int hor = -1; // 1 horario, -1antihorario
	
	double heading;
	double hdestinyU = 30;
	
	double hdestiny;
	
	if(hdestinyU > 180){
		hdestiny = hdestinyU-180;
	}else{
		hdestiny = hdestinyU+180;
	}
	
	//Kc, Ti, Td, interval
	PID controller(1.0, 0.0, 0.0, RATE);
	//Analog input from 0.0 to 3.3V
	controller.setInputLimits(0,181);
	//Pwm output from 0.0 to 1.0
	controller.setOutputLimits(0, 1.6);
	//If there's a bias.
	//controller.setBias(0.3);
	controller.setMode(AUTO_MODE);
	//We want the process variable to be 1.7V
	controller.setSetPoint(180);
	

	RoboClaw *roboclaw = new RoboClaw;
	roboclaw->SetMinVoltageMainBattery(address,2);
	roboclaw->ForwardMixed(address,veloBase);

	
	AHRS todo;    
    usleep(20);

    while(1){

    	todo.updateIMU();

    	//heading = ToDeg(todo.yaw);
    	heading = todo.compass.heading();
    	
    	//cout << "Tild: " << todo.MAG_Heading_Deg << endl;
        //cout << "No Tild: " << todo.compass.heading() << endl;

    	cout << "heading: " << heading << endl;
   	
    	//Revisa tolerancia
		if(abs(heading-hdestiny) > tolerancia){
		
			float diff = abs(heading-hdestiny);
			
			if(diff > 180 ){
				diff = abs(360-diff);
			}

			controller.setProcessValue(diff);
			porcen = controller.compute();

		    //caso giro horario
			if( ((hor*(heading-hdestiny) < 0)&&(abs(heading-hdestiny) < 180)) || ( (hor*(heading-hdestiny) > 0)&&(abs(heading-hdestiny) > 180)) ){
			    //cout << "hdestiny: " << hdestiny << "heading: " << heading << endl;
				//cout << "caso 1: " << abs(heading-hdestiny) << endl;

				cout << "caso 1 -> Motor1 = " << (veloBase-(porcen/2)*veloBase) << " Motor2 = " << veloBase+(porcen/2)*veloBase << endl;

				roboclaw->ForwardM1(address, veloBase-(porcen/2)*veloBase);
				roboclaw->ForwardM2(address, veloBase+(porcen/2)*veloBase);

				continue;						
			}
			//caso giro antihorario
			if( ((hor*(heading-hdestiny) > 0)&&(abs(heading-hdestiny) < 180)) || ( (hor*(heading-hdestiny) < 0)&&(abs(heading-hdestiny) > 180)) ){
				//cout << "hdestiny: " << hdestiny << "heading: " << heading << endl;
				//cout << "caso 2: " << abs(heading-hdestiny) << endl;

				cout << "caso 2 -> Motor1 = " << veloBase-(porcen/2)*veloBase << " Motor2 = " << veloBase+(porcen/2)*veloBase << endl;
				

				roboclaw->ForwardM1(address, veloBase+(porcen/2)*veloBase);
				roboclaw->ForwardM2(address, veloBase-(porcen/2)*veloBase);

				continue;
			}
		
		}else{
		
			roboclaw->ForwardMixed(address,veloBase);
		}
		
		cout << "porcen: " << porcen << endl;
    
    }
    
}  




  
    


