#include "GPS.h"


//
// Constructor
//
GPS::GPS(){
}

//
// Destructor
//
GPS::~GPS(){
}

void GPS::getHeading(){

	getData();
	DMStoDD();
	getHeadingDistance();

}

void GPS::DMStoDD(){

	lon = longitudeDeg + (longitudeMin/60) + (longitudeSeg/3600);
	lat = latitudeDeg + (latitudeMin/60) + (latitudeSeg/3600);

}

void GPS::getHeadingDistance(){

	Geodesic geod(Constants::WGS84_a(), 1/298.257223563);
	geod.Inverse(lat, lon, deslat, deslon, distancia, headingP1, headingP2);

}


void GPS::getData(){
	
	//-- ASCII string to send through the serial port
	const string CMD= "1";

	//-- Open the serial port
	//-- Serial port configuration: 9600 baud, 8N1
	//SerialPort serial_compass("/dev/ttyO4");
	SerialPort serial_GPS("/dev/ttyO4");

	bool writeFail = false;

	try {
	  serial_GPS.Open(SerialPort::BAUD_57600,
		               SerialPort::CHAR_SIZE_8,
		               SerialPort::PARITY_NONE,
		               SerialPort::STOP_BITS_1,
		               SerialPort::FLOW_CONTROL_NONE);
	}

	catch (SerialPort::OpenFailed E) {
	  cout << "Error opening the serial port" << endl;
	  exit (EXIT_FAILURE);
	}

	bool notValid;
	SerialPort::DataBuffer bufferGPS;
	int count = 0;

	do{

	notValid = false;
	count++;

	//-- Wait for the received data
	  try {
		  serial_GPS.Read(bufferGPS,42,500);
	  }
	  catch (SerialPort::ReadTimeout E) {
		  cout << "TIMEOUT!" << endl;
	  latitudeDeg = -1;
	  latitudeMin = -1;
	  latitudeSeg = -1;
	  longitudeDeg = -1;
	  longitudeMin = -1;
	  longitudeSeg = -1;
	  exit (EXIT_FAILURE);
	  }

	if(bufferGPS[0] != '$'){
	  notValid = true;    
	}
	else if( bufferGPS[22] != '.' ){
	  notValid = true;    
	}
	else if( bufferGPS[35] != '.'){
	  notValid = true;    
	}
	} while(notValid && (count <= 5));

	stringstream ss1;
	string tempLatitudeDeg;
	stringstream ss2;
	string tempLatitudeMin;
	stringstream ss3;
	string tempLatitudeSeg;
	stringstream ss4;
	string tempLongitudeDeg;
	stringstream ss5;
	string tempLongitudeMin;
	stringstream ss6;
	string tempLongitudeSeg;

	if(notValid){
	  latitudeDeg = -1;
	  latitudeMin = -1;
	  latitudeSeg = -1;
	  longitudeDeg = -1;
	  longitudeMin = -1;
	  longitudeSeg = -1;
	}
	else {


	for(int ii=18; ii < 20; ii++) {
	  ss1 << bufferGPS[ii];
	}
	ss1 >> tempLatitudeDeg;
	latitudeDeg = atof(tempLatitudeDeg.c_str());

	for(int ii=20; ii < 22; ii++) {
	  ss2 << bufferGPS[ii];
	}
	ss2 >> tempLatitudeMin;
	latitudeMin = atof(tempLatitudeMin.c_str());

	for(int ii=23; ii < 27; ii++) {
	  ss3 << bufferGPS[ii];
	}
	ss3 >> tempLatitudeSeg;
	latitudeSeg = 0.006*atof(tempLatitudeSeg.c_str());

	for(int ii=30; ii < 33; ii++) {
	  ss4 << bufferGPS[ii];
	}
	ss4 >> tempLongitudeDeg;
	longitudeDeg = atof(tempLongitudeDeg.c_str());

	for(int ii=33; ii < 35; ii++) {
	  ss5 << bufferGPS[ii];
	}
	ss5 >> tempLongitudeMin;
	longitudeMin = atof(tempLongitudeMin.c_str());

	for(int ii=36; ii < 40; ii++) {
	  ss6 << bufferGPS[ii];
	}
	ss6 >> tempLongitudeSeg;
	longitudeSeg = 0.006*atof(tempLongitudeSeg.c_str());
	}

	//serial_compass.Close();
	serial_GPS.Close();
}

