String Roy;
int Jx=0;
int Jy=0;
char buffer[10];

void setup() {
    Serial.begin(9600); // Default connection rate for my BT module
    
}

void loop() {
    if(Serial.available() > 0){
      
      String output = Serial.readStringUntil(';');
      
      
      if(output.substring(0,2) == "CJ"){
        int commaIndex = output.indexOf(',');
        int SecondcommaIndex = output.indexOf(',', commaIndex+1);
        
        output.substring(commaIndex+1,SecondcommaIndex).toCharArray(buffer,10);
        Jx = atof(buffer)*1000;
        
        output.substring(SecondcommaIndex+1).toCharArray(buffer,10);
        Jy = atof(buffer)*1000;
        
        int Jy_constrained = constrain(Jy, 0, 999);
        
        int centerPWM = map(Jy_constrained,0,999,0,255);
        int turn = map(Jx,-999,999,-50,50);
        
        analogWrite(9, constrain(centerPWM-turn,0,255));
        analogWrite(10, constrain(centerPWM+turn,0,255));
        
        /*Serial.print("Motor1: ");
        Serial.print(constrain(centerPWM+turn,0,255));
        Serial.print(" Motor2: ");
        Serial.println(constrain(centerPWM-turn,0,255));*/
      }
      
      else{
        analogWrite(9, 0);
        analogWrite(10, 0);  
      }
    }
}

